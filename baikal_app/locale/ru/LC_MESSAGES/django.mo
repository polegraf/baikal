��    t      �      \      \     ]     p     �     �     �     �     �  	   �  
   �     �     �     �     �          	                /     7     C     S     b     r  *   ~     �     �     �     �     �     �     	     	  	   !	  
   +	     6	     <	  $   H	  (   m	     �	     �	     �	     �	     �	     �	     

     &
     <
     J
     P
     U
  	   Z
  	   d
     n
  9   w
     �
     �
     �
  	   �
     �
     �
     �
  	             +  
   =     H     O     \  	   l     v     �     �     �     �     �  
   �  	   �     �     �  *        7     =     W  +   t     �     �     �  	   �     �     �     �     �     �     �          
       
   +     6     J     V     _  
   o  	   z     �     �     �     �     �     �     �     �     �  
   �     �  �    '   �  !   �     !     .     M     g     �     �     �  8   �  8        L     l     u     ~  #   �  #   �     �     �  "         $  $   E     j  J   {     �     �      �           /  $   P      u  $   �  
   �  )   �     �  )     <   1  B   n  +   �  )   �  2     *   :  /   e  9   �  *   �  &   �     !     3  
   <     G     T     l     �  R   �      �  "     
   0     ;  2   O  2   �     �     �  %   �  '        8     Q  %   ^  !   �  #   �  %   �     �  $        0  #   A     e  #   �     �  2   �  6   �  J   '     r  2   �  6   �  J   �     8  %   S     y     �  0   �     �     �  !   �          )     <     I  /   i     �  '   �     �     �               8     P     a  &     5   �  
   �     �  $   �     $      <     ]     l   !Technical layout! Banner Settings Black Bottom Link Bottom Link Group Bottom Link Groups Bottom Links Bottom ad Bottom ads Bubble Bubble content Button title Circle Color Contacts Contacts page Contacts pages Content Coordinates Culture: Cherry Culture: Green Culture: Orange Description FACTS ABOUT BAIKAL LAKE & MOUNTAIN RESORT: Graphics only HTML Description HTML Keywords HTML description HTML keywords Hotspot Settings Hotspot for Icon Star Text Icon Text Icon Title Image Image Title Image for the info block on the left Image on the right hand side of the page Image replacement Image title Image title color Indicated in pixels Info block main text Info block main text color Info block text in the star Info block title text Language Code Layer Left Link Link Text Link text Location Location should be indicated in the following format: x;y Main banner Main banners Map Map Layer Map Layer Hotspot Map Layer Hotspots Map Layer Settings Map layer Map layer marker Map layer markers Map layers Marker Marker Image Marker Settings Marker at Marker direction Nature: Blue Nature: Brown Page Page Settings Page address Page title Page type Page with text and a map Page with text and graphics Page with two columns of text and graphics Pages Pages with text and a map Pages with text and graphics Pages with two columns of text and graphics Polygon Position Publish Rectangle Redirection url Right Section Section Settings Sections Settings Shape Short title Short title for menu Subsection Subsection Settings Subsections Subtitle Text & graphics Text & map Text only Title Title [hint] Top menu width Two column text & graphics White X shift X shift right Y shift Y shift down created by for map Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-11-10 15:18+0300
PO-Revision-Date: 2011-11-10 16:18
Last-Translator:   <kvs@tramplin.tv>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: 
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Translated-Using: django-rosetta 0.6.0
 !Техническая вёрстка! Настройки баннера Чёрный Ссылка в подвале Группа ссылок Группы ссылок Ссылки в подвале Рекламный блок Рекламные блоки Содержимое всплывающего блока Содержимое всплывающего блока Краткое название Круг Цвет Контакты Страница "Контакты" Страницы "Контакты" Содержимое Координаты Культура: вишнёвый Культура: зелёный Культура: оранжевый Описание ФАКТЫ ОБ ОЗЕРЕ БАЙКАЛ И О ГОРНОМ КУРОРТЕ: Только графика HTML описание HTML Ключевые слова HTML описание HTML Ключевые слова Настройки зоны слоя Активная зона для Текст на "звёздочке" Текст Заголовок изображения Изображение Заголовок изображения Изображение для инфо-блока слева Изображение в правой части страницы Замещающее изображение Заголовок изображения Цвет заголовка изображения Указывается в пикселах Основной текст инфо-блока Цвет текста главного инфоблока Текст внутри звёздочки Заголовок инфо-блока Код языка Слой Слева Ссылка Текст ссылки Текст ссылки Координаты Координаты необходимо указывать в формате x;y Баннер на главной Баннеры на главной Карта Слой карты Активная зона на слое карты Активные зоны на слое карты Настройки слоя Слой на карте Маркер на слое карты Маркеры на слое карты Слои на карте Маркер Изображение маркера Настройки маркера Координаты маркера Направление маркера Природа: синий Природа: коричневый Страница Настройки Страницы Адрес страницы Заголовок страницы Тип страницы Страница с текстом и картой Страница с текстом и графикой Страница с текстом в 2 колонки и графикой Страницы Страницы с текстом и картой Страницы с текстом и графикой Страницы с текстом в 2 колонки и графикой Многоугольник Порядок отображения Опубликовать Прямоугольник Адрес для перенаправления Справа Раздел Настройки раздела Разделы Настройки Контур Краткое название Краткое название для меню Подраздел Настройки подраздела Подразделы Подзаголовок Текст и графика Текст и карта Только текст Название Заголовок [хинт] Ширина верхнего меню Текст в две колонки и графика Белый Смещение по X Смещение вправо по X Смещение по Y Смещение вниз по Y создано для карты 