# Create your views here.
import base64
import hashlib
import os
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response, get_object_or_404, get_list_or_404
from django.template import loader
from django.template.context import RequestContext, Context
from baikal_app.models import *
from cairotext.text import text_as_graphic
from textograf.text import text_as_graphic as pil_text_as_graphic

def common_stuff(request, custom_stuff=None):
	if not custom_stuff: custom_stuff = {}
	sections = Section.objects.filter(publish=True).order_by('position')
	bottom_picture_ads = BottomPictureAd.objects.filter(publish=True)
	bottom_link_groups = BottomLinkGroup.objects.filter(publish=True)
	languages = EnabledLanguage.objects.filter(publish=True)
	return RequestContext(request, dict(sections=sections, languages=languages,
										bottom_picture_ads=bottom_picture_ads,
										bottom_link_groups=bottom_link_groups,
										**custom_stuff))

def index(request):
	banner = MainBanner.objects.filter(publish=True).order_by('position')[0]
	banners = MainBanner.objects.filter(publish=True).order_by('-position')
	request.session['current_banner'] = banner.position
	return render_to_response('index.html', context_instance=common_stuff(request, {'banner': banner, 'banners': banners}))

def next_banner(request):
	cb = request.session.get('current_banner', -1)
	try:
		banner = MainBanner.objects.filter(position__gt=cb).filter(publish=True).order_by('position')[0]
	except IndexError:
		banner = MainBanner.objects.filter(publish=True).order_by('position')[0]
	request.session['current_banner'] = banner.position
	return render_to_response('ajax_main_banner.html', context_instance=common_stuff(request, {'banner': banner}))

def previous_banner(request):
	cb = request.session.get('current_banner', 65535)
	try:
		banner = MainBanner.objects.filter(position__lt=cb).order_by('-position').filter(publish=True)[0]
	except IndexError:
		banner = MainBanner.objects.filter(publish=True).order_by('-position')[0]
	request.session['current_banner'] = banner.position
	return render_to_response('ajax_main_banner.html', context_instance=common_stuff(request, {'banner': banner}))


def banner(request, id):
	try:
		b = MainBanner.objects.get(pk=int(id))
	except MainBanner.DoesNotExist:
		b = MainBanner.objects.filter(publish=True).order_by('position')[0]
	request.session['current_banner'] = b.position
	return render_to_response('ajax_main_banner.html', context_instance=common_stuff(request, {'banner': b }))

def handle_page(request, alias=None, page=None, ajax=False):
	if not page:
		page = get_object_or_404(Page, alias=alias)
	if request.user.is_anonymous() and not page.publish:
		raise Http404
	if page.sub_section:
		pages = Page.objects.filter(sub_section=page.sub_section).order_by('-position')
		if request.user.is_anonymous():
			pages = pages.filter(publish=True)
	else:
		pages = None
	hide_arrows = not page.sub_section or pages.count() == 1
	request.session['current_page'] = page.pk
	f = None
	try:
		f = eval('_page_%s' % page.page_type)
	except NameError:
		template = 'ajax_%s.html' % page.page_type if ajax else 'page_%s.html' % page.page_type
	if f:
		return f(request, page, ajax, hide_arrows, pages)
	else:
		return render_to_response(template, context_instance=common_stuff(request, {'page': page, 'pages': pages,
																				  'hide_arrows': hide_arrows}))


def previous_page(request, id):
	page = get_object_or_404(Page, pk=int(id))
	if not page.sub_section:
		raise Http404
	try:
		previous = Page.objects.filter(sub_section=page.sub_section).filter(position__lt=page.position).order_by('-position').\
				   filter(publish=True)[0]
	except IndexError:
		previous = Page.objects.filter(sub_section=page.sub_section).filter(publish=True).order_by('-position')[0]
	return handle_page(request, page=previous, ajax=True)


def next_page(request, id):
	page = get_object_or_404(Page, pk=int(id))
	if not page.sub_section:
		raise Http404
	try:
		next = Page.objects.filter(sub_section=page.sub_section).filter(position__gt=page.position).order_by('position').\
			filter(publish=True)[0]
	except IndexError:
		next = Page.objects.filter(sub_section=page.sub_section).order_by('position').filter(publish=True)[0]
	return handle_page(request, page=next, ajax=True)

def _page_text_map(request, page, ajax, hide_arrows, pages):
	template = 'ajax_text_map.html' if ajax else 'page_text_map.html'
	ml = list(MapLayer.objects.filter(page=page).filter(publish=True).order_by('position'))
	if not ml:
		raise Http404
	return render_to_response(template, context_instance=common_stuff(request, {'map_layers': ml,
																				  'page': page, 'pages': pages,
																				  'hide_arrows': hide_arrows}))

def get_banner_id(request):
	if 'current_banner' in request.session:
		b = MainBanner.objects.get(position__exact=request.session['current_banner'])
	else:
		b = MainBanner.objects.filter(publish=True).order_by('position')[0]
	result = b.pk
	return HttpResponse(result)


def page_title(request, id):
	page = get_object_or_404(Page, pk=int(id))
	font_size = 31
	if request.LANGUAGE_CODE in ('en', 'de'):
		font_name = 'OfficinaSerifBlackC.otf'
		extra_interline = 8
	else:
		extra_interline = 8
		if request.LANGUAGE_CODE == 'zh':
			font_name = 'FZY4JW.TTF'
		elif request.LANGUAGE_CODE == 'ko':
			font_name = 'HeadlineA.ttf'
		elif request.LANGUAGE_CODE == 'ru':
			font_name = 'OfficinaSerifBlackC.otf'
			font_size = 32
		else:
			font_name = 'DroidSansFallback.ttf'
	return text_as_graphic(page.title.upper(), font_file=font_name, size=font_size,
						   extra_interline=extra_interline, bg=0x00ececed)

def page_subtitle(request, id):
	page = get_object_or_404(Page, pk=int(id))
	european = request.LANGUAGE_CODE in ('en', 'ru', 'de')
	font_name = 'OSR45__C.TTF' if european else 'DroidSansFallback.ttf'
	return text_as_graphic(page.content, font_file=font_name, word_wrap=25, size=20, extra_interline=-2, bg=0x00ececed)


def page(request, id):
	page = get_object_or_404(Page, publish=True, pk=int(id))
	return handle_page(request, page=page, ajax=True)


def get_page_id(request):
	return HttpResponse(request.session.get('current_page', -1))


def slide_title(request, id):
	extra_interline = 5
	font_size = 48
	if request.LANGUAGE_CODE in ('en', 'de'):
		font_name = 'OfficinaSerifBlackC.otf'
		extra_interline = 4
	else:
		if request.LANGUAGE_CODE == 'zh':
			font_name = 'FZY4JW.TTF'
			extra_interline = 2
		elif request.LANGUAGE_CODE == 'ru':
			font_name = 'OfficinaSerifBlackC.otf'#'officina_ctt_bold.ttf'
			extra_interline = 3
		elif request.LANGUAGE_CODE == 'ko':
			font_name = 'HeadlineA.ttf'
		else:
			font_name = 'DroidSansFallback.ttf'
	p = get_object_or_404(Page, publish=True, pk=int(id))
	return text_as_graphic(p.image_title.upper(), fg=int(p.image_title_color, 16), font_file=font_name,
						   size=font_size, image_size=(906,648,), align='right', padding=(33, 33,), bg=0x00999999,
						   extra_interline=extra_interline)


def iframe_google(request):
	from json import dumps as jd
	mrks = GoogleMarker.objects.filter(publish=True)
	markers = []
	t = loader.get_template('part_google_map.html')
	for m in mrks:
		markers.append({'lng': m.lng, 'lat': m.lat, 'description': jd(t.render(RequestContext(request, {'obj': m})))})
	return render_to_response('google.html', context_instance=RequestContext(request, {'markers': markers}))


def slide_banner(request, id):
	this_banner = get_object_or_404(MainBanner, pk=int(id))
	if not this_banner.icon:
		raise Http404
	try:
		hashdata = u'%s%s%s%s%s' % (this_banner.icon.datetime.ctime(),
													 this_banner.icon_title,
							   						 this_banner.icon_text,
							   						 this_banner.icon_star_text,
													 this_banner.icon_text_color)
	except TypeError:
		error = 'File %s not found' % this_banner.icon
		raise Exception(error)

	md = hashlib.md5(base64.encodestring(hashdata.encode('utf-8')))
	filename = u'%s%d%s.png' % (request.LANGUAGE_CODE, int(this_banner.pk), md.hexdigest())
	full_path = os.path.join(os.path.dirname(__file__), '..', 'media', 'img', 'banners', filename)
	if not os.path.exists(full_path):
		from graphics import cairo_make_banner
		b = cairo_make_banner(open(this_banner.icon.path_full, 'rb'), this_banner.icon_title, this_banner.icon_text,
							this_banner.icon_star_text, request.LANGUAGE_CODE, this_banner.icon_text_color)
		b.save(full_path)
	return HttpResponseRedirect('/media/img/banners/%s' % filename)


def main_picture(request, id):
	this_picture = get_object_or_404(BottomPictureAd, id=int(id))
	if not this_picture.image:
		raise Http404
	hashdata = u'%s%s' % (this_picture.image.datetime.ctime(), this_picture.image.filename)
	filename = u'%s%d%s.png' % (request.LANGUAGE_CODE, int(this_picture.pk),
								hashlib.md5(base64.encodestring(hashdata.encode('utf-8'))).hexdigest()
							  )
	full_path = os.path.join(os.path.dirname(__file__), '..', 'media', 'img', 'main_pics', filename)
	if not os.path.exists(full_path):
		from graphics import wet_floor
		try:
			b = wet_floor(open(this_picture.image.path_full, 'rb'))
		except:
			raise Http404
		b.save(full_path)
	return HttpResponseRedirect('/media/img/main_pics/%s' % filename)