# coding: utf-8

from django.contrib import admin
from django.forms.models import ModelForm
from django.forms.widgets import Textarea, HiddenInput
from baikal_app.models import *
from django.utils.translation import ugettext_lazy as _

class BaseAdmin(admin.ModelAdmin):
	list_display = ('publish', '__unicode__',)
	list_display_links = ('__unicode__',)

class HTMLMixin:
	class Media:
		js = ['/admin_media/tinymce/jscripts/tiny_mce/tiny_mce.js',
			  '/admin_media/tinymce_setup/tinymce_setup.js']

class SortableMixin:
	sortable_field_name = 'position'
	class Meta:
		widgets = {
			'position': HiddenInput(),
		}

class SortableTabularInline(SortableMixin, admin.TabularInline):
	pass

class SortableStackedInline(SortableMixin, admin.StackedInline):
	pass

class SortableAdmin(BaseAdmin):
	"""
	Allows re-ordering items in the main list
	"""
	list_display = ('publish', '__unicode__', 'position')
	list_display_links = ('__unicode__',)
	list_editable = ('position',)

	class Media:
		js = [
			'/admin_media/js/admin_list_reorder.js',
		]

class PageForm(ModelForm):
	class Meta:
		model = Page
		widgets = {
			'image_title_en': Textarea(attrs={'class': 'mceNoEditor'}),
			'image_title_ru': Textarea(attrs={'class': 'mceNoEditor'}),
			'image_title_de': Textarea(attrs={'class': 'mceNoEditor'}),
			'image_title_ko': Textarea(attrs={'class': 'mceNoEditor'}),
			'image_title_zh': Textarea(attrs={'class': 'mceNoEditor'}),
			'title_en': Textarea(attrs={'class': 'mceNoEditor', 'rows': '3'}),
			'title_ru': Textarea(attrs={'class': 'mceNoEditor', 'rows': '3'}),
			'title_de': Textarea(attrs={'class': 'mceNoEditor', 'rows': '3'}),
			'title_ko': Textarea(attrs={'class': 'mceNoEditor', 'rows': '3'}),
			'title_zh': Textarea(attrs={'class': 'mceNoEditor', 'rows': '3'}),
		}

class TechPageForm(ModelForm):
	class Meta:
		model = Page
		widgets = {
			'image_title_en': Textarea(attrs={'class': 'mceNoEditor'}),
			'image_title_ru': Textarea(attrs={'class': 'mceNoEditor'}),
			'image_title_de': Textarea(attrs={'class': 'mceNoEditor'}),
			'image_title_ko': Textarea(attrs={'class': 'mceNoEditor'}),
			'image_title_zh': Textarea(attrs={'class': 'mceNoEditor'}),
			'content_en': Textarea(attrs={'class': 'mceNoEditor'}),
			'content_ru': Textarea(attrs={'class': 'mceNoEditor'}),
			'content_de': Textarea(attrs={'class': 'mceNoEditor'}),
			'content_ko': Textarea(attrs={'class': 'mceNoEditor'}),
			'content_zh': Textarea(attrs={'class': 'mceNoEditor'}),
			'title_en': Textarea(attrs={'class': 'mceNoEditor', 'rows': '3'}),
			'title_ru': Textarea(attrs={'class': 'mceNoEditor', 'rows': '3'}),
			'title_de': Textarea(attrs={'class': 'mceNoEditor', 'rows': '3'}),
			'title_ko': Textarea(attrs={'class': 'mceNoEditor', 'rows': '3'}),
			'title_zh': Textarea(attrs={'class': 'mceNoEditor', 'rows': '3'}),
		}

class PageAdmin(HTMLMixin, SortableAdmin):
	list_display = ('publish', '__unicode__', 'sub_section', 'position')
	form = PageForm
	list_filter = ('sub_section', 'page_type', 'publish')
	fieldsets = (
		(
			_('Subsection'), {
				'fields': ('sub_section',),
			}
		),
		(
			_('Short title'), {
				'fields': ('short_title_en', 'short_title_ru', 'short_title_de', 'short_title_ko', 'short_title_zh'),
				'classes': ('collapse', 'closed'),
			}
		),
		(
			_('Title'), {
				'fields': ('title_en', 'title_ru', 'title_de', 'title_ko', 'title_zh'),
				'classes': ('collapse', 'closed'),
			}
		),
		(
			_('HTML Keywords'), {
				'fields': ('html_keywords_en', 'html_keywords_ru', 'html_keywords_de', 'html_keywords_ko',
						   'html_keywords_zh'),
				'classes': ('collapse', 'closed'),
			}
		),
		(
			_('HTML Description'), {
				'fields': ('html_description_en', 'html_description_ru', 'html_description_de', 'html_description_ko',
						   'html_description_zh'),
				'classes': ('collapse', 'closed'),
			}
		),
		(
			_('Content'), {
				'fields': ('content_en', 'content_ru', 'content_de', 'content_ko', 'content_zh'),
				'classes': ('collapse', 'closed'),
			}
		),
		(
			_('Image Title'), {
				'fields': ('image_title_en', 'image_title_ru', 'image_title_de', 'image_title_ko', 'image_title_zh'),
				'classes': ('collapse', 'closed'),
			}
		),
		(
			_('Page Settings'), {
				'fields': ('image_en', 'page_type', 'alias', 'publish'),
				'classes': ('collapse', 'open'),
			}
		),
	)

class TechPageAdmin(PageAdmin):
	form = TechPageForm

class PageProxyAdmin(PageAdmin):
	list_filter = ('sub_section', 'publish')
	fieldsets = (
		(
			_('Subsection'), {
				'fields': ('sub_section',),
			}
		),
		(
			_('Short title'), {
				'fields': ('short_title_en', 'short_title_ru', 'short_title_de', 'short_title_ko', 'short_title_zh'),
				'classes': ('collapse', 'closed'),
			}
		),
		(
			_('Title'), {
				'fields': ('title_en', 'title_ru', 'title_de', 'title_ko', 'title_zh'),
				'classes': ('collapse', 'closed'),
			}
		),
		(
			_('HTML Keywords'), {
				'fields': ('html_keywords_en', 'html_keywords_ru', 'html_keywords_de', 'html_keywords_ko',
						   'html_keywords_zh'),
				'classes': ('collapse', 'closed'),
			}
		),
		(
			_('HTML Description'), {
				'fields': ('html_description_en', 'html_description_ru', 'html_description_de', 'html_description_ko',
						   'html_description_zh'),
				'classes': ('collapse', 'closed'),
			}
		),
		(
			_('Content'), {
				'fields': ('content_en', 'content_ru', 'content_de', 'content_ko', 'content_zh'),
				'classes': ('collapse', 'closed'),
			}
		),
		(
			_('Image Title'), {
				'fields': ('image_title_en', 'image_title_ru', 'image_title_de', 'image_title_ko', 'image_title_zh'),
				'classes': ('collapse', 'closed'),
			}
		),
		(
			_('Page Settings'), {
				'fields': ('image_en', 'alias', 'publish'),
				'classes': ('collapse', 'open'),
			}
		),
	)

class PageTextGraphicAdmin(PageProxyAdmin):
	fieldsets = (
		(
			_('Subsection'), {
				'fields': ('sub_section',),
			}
		),
		(
			_('Short title'), {
				'fields': ('short_title_en', 'short_title_ru', 'short_title_de', 'short_title_ko', 'short_title_zh'),
				'classes': ('collapse', 'closed'),
			}
		),
		(
			_('Title'), {
				'fields': ('title_en', 'title_ru', 'title_de', 'title_ko', 'title_zh'),
				'classes': ('collapse', 'closed'),
			}
		),
		(
			_('HTML Keywords'), {
				'fields': ('html_keywords_en', 'html_keywords_ru', 'html_keywords_de', 'html_keywords_ko',
						   'html_keywords_zh'),
				'classes': ('collapse', 'closed'),
			}
		),
		(
			_('HTML Description'), {
				'fields': ('html_description_en', 'html_description_ru', 'html_description_de', 'html_description_ko',
						   'html_description_zh'),
				'classes': ('collapse', 'closed'),
			}
		),
		(
			_('Content'), {
				'fields': ('content_en', 'content_ru', 'content_de', 'content_ko', 'content_zh'),
				'classes': ('collapse', 'closed'),
			}
		),
		(
			_('Image Title'), {
				'fields': ('image_title_en', 'image_title_ru', 'image_title_de', 'image_title_ko', 'image_title_zh'),
				'classes': ('collapse', 'closed'),
			}
		),
		(
			_('Page Settings'), {
				'fields': ('image_en', 'image_title_color', 'alias', 'publish'),
				'classes': ('collapse', 'open'),
			}
		),
	)

	def queryset(self, request):
		return self.model.objects.filter(page_type='text_graphic')

class PageTwoColumnsTextGraphicAdmin(PageProxyAdmin):

	def queryset(self, request):
		return self.model.objects.filter(page_type='twocoltext_graphic')

class PageContactsAdmin(PageProxyAdmin):

	def queryset(self, request):
		return self.model.objects.filter(page_type='contacts')

class MapLayerInline(SortableStackedInline):
	model = MapLayer
	extra = 0
	fieldsets = (
		(
			_('Title'), {
				'fields': ('title_en', 'title_ru', 'title_de', 'title_ko', 'title_zh'),
				'classes': ('collapse', 'closed'),
				},
		),
		(
			_('Image'), {
				'fields': ('image_en', 'image_ru', 'image_de', 'image_ko', 'image_zh'),
				'classes': ('collapse', 'closed'),
				},
		),
		(
			_('X shift'), {
				'fields': ('shift_x_en', 'shift_x_ru', 'shift_x_de', 'shift_x_ko', 'shift_x_zh'),
				'classes': ('collapse', 'closed'),
				},
		),
		(
			_('Y shift'), {
				'fields': ('shift_y_en', 'shift_y_ru', 'shift_y_de', 'shift_y_ko', 'shift_y_zh'),
				'classes': ('collapse', 'closed'),
				},
		),
		(
			_('Map Layer Settings'), {
				'fields': ('page', 'publish', 'position'),
				},
		),
	)


class PageTextMapAdmin(PageProxyAdmin):
	inlines = (MapLayerInline,)
	fieldsets = (
		(
			_('Subsection'), {
				'fields': ('sub_section',),
			}
		),
		(
			_('Short title'), {
				'fields': ('short_title_en', 'short_title_ru', 'short_title_de', 'short_title_ko', 'short_title_zh'),
				'classes': ('collapse', 'closed'),
			}
		),
		(
			_('Title'), {
				'fields': ('title_en', 'title_ru', 'title_de', 'title_ko', 'title_zh'),
				'classes': ('collapse', 'closed'),
			}
		),
		(
			_('HTML Keywords'), {
				'fields': ('html_keywords_en', 'html_keywords_ru', 'html_keywords_de', 'html_keywords_ko',
						   'html_keywords_zh'),
				'classes': ('collapse', 'closed'),
			}
		),
		(
			_('HTML Description'), {
				'fields': ('html_description_en', 'html_description_ru', 'html_description_de', 'html_description_ko',
						   'html_description_zh'),
				'classes': ('collapse', 'closed'),
			}
		),
		(
			_('Content'), {
				'fields': ('content_en', 'content_ru', 'content_de', 'content_ko', 'content_zh'),
				'classes': ('collapse', 'closed'),
			}
		),
		(
			_('Image Title'), {
				'fields': ('image_title_en', 'image_title_ru', 'image_title_de', 'image_title_ko', 'image_title_zh'),
				'classes': ('collapse', 'closed'),
			}
		),
		(
			_('Background Image'), {
				'fields': ('image_en', 'image_ru', 'image_de', 'image_ko', 'image_zh',),
				'classes': ('collapse', 'closed'),
				},
		),
		(
			_('Page Settings'), {
				'fields': ('alias', 'publish'),
				'classes': ('collapse', 'open'),
			}
		),
	)

	def queryset(self, request):
		return self.model.objects.filter(page_type='text_map')

class MainBannerAdmin(SortableAdmin):
	list_filter = ('publish',)
	fieldsets = (
	(
		(
			_('Title'), {
				'fields': ('title_en', 'title_ru', 'title_de', 'title_ko', 'title_zh'),
				'classes': ('collapse', 'closed',),
				},
		),
		(
			_('Subtitle'), {
				'fields': ('subtitle_en', 'subtitle_ru', 'subtitle_de', 'subtitle_ko', 'subtitle_zh'),
				'classes': ('collapse', 'closed'),
				},
		),
		(
			_('Icon Title'), {
				'fields': ('icon_title_en', 'icon_title_ru', 'icon_title_de', 'icon_title_ko', 'icon_title_zh'),
				'classes': ('collapse', 'closed'),
				},
		),
		(
			_('Icon Text'), {
				'fields': ('icon_text_en', 'icon_text_ru', 'icon_text_de', 'icon_text_ko', 'icon_text_zh'),
				'classes': ('collapse', 'closed'),
				},
		),
		(
			_('Icon Star Text'), {
				'fields': ('icon_star_text_en', 'icon_star_text_ru', 'icon_star_text_de', 'icon_star_text_ko', 'icon_star_text_zh'),
				'classes': ('collapse', 'closed'),
				},
		),
		(
			_('Banner Settings'), {
				'fields': ('image', 'icon', 'icon_text_color', 'url', 'publish'),
				'classes': ('collapse', 'open'),
				},
			),
		)
	)

class SectionAdmin(SortableAdmin):
	fieldsets = (
	(
		_('Title'), {
			'fields': ('title_en', 'title_ru', 'title_de', 'title_ko', 'title_zh',),
			'classes': ('collapse', 'closed'),
			},
		),
		(
		_('Section Settings'), {
			'fields': ('publish', 'width'),
			'classes': ('collapse', 'open'),
			},
		),
	)

class SubSectionAdmin(BaseAdmin):
	list_display = ('publish', '__unicode__', 'section', 'position')
	fieldsets = (
	(
		_('Title'), {
			'fields': ('title_en', 'title_ru', 'title_de', 'title_ko', 'title_zh'),
			'classes': ('collapse', 'closed'),
			},
		),
		(
		_('Subsection Settings'), {
			'fields': ('section', 'publish',),
			'classes': ('collapse', 'open'),
			},
		),
	)

class MapLayerAdmin(SortableAdmin):
	list_filter = ('page', 'publish',)
	fieldsets = (
		(
			_('Title'), {
				'fields': ('title_en', 'title_ru', 'title_de', 'title_ko', 'title_zh'),
				'classes': ('collapse', 'closed'),
				},
		),
		(
			_('Image'), {
				'fields': ('image_en', 'image_ru', 'image_de', 'image_ko', 'image_zh'),
				'classes': ('collapse', 'closed'),
				},
		),
		(
			_('X shift'), {
				'fields': ('shift_x_en', 'shift_x_ru', 'shift_x_de', 'shift_x_ko', 'shift_x_zh'),
				'classes': ('collapse', 'closed'),
				},
		),
		(
			_('Y shift'), {
				'fields': ('shift_y_en', 'shift_y_ru', 'shift_y_de', 'shift_y_ko', 'shift_y_zh'),
				'classes': ('collapse', 'closed'),
				},
		),
		(
			_('Map Layer Settings'), {
				'fields': ('page', 'publish'),
				'classes': ('collapse', 'open'),
				},
		),
	)

class MapLayerHotspotAdmin(BaseAdmin, HTMLMixin):
	list_filter = ('map_layer', 'publish')
	fieldsets = (
		(
			_('Hotspot Settings'), {
				'fields': ('map_layer', 'publish', 'shape'),
				'classes': ('collapse', 'open'),
				},
		),
		(
			_('Coordinates'), {
				'fields': ('coords_en', 'coords_ru', 'coords_de', 'coords_ko', 'coords_zh'),
				'classes': ('collapse', 'closed'),
				},
		),
		(
			_('Image replacement'), {
				'fields': ('image_en', 'image_ru', 'image_de', 'image_ko', 'image_zh'),
				'classes': ('collapse', 'closed'),
				},
		),
	)

class MapMarkerForm(ModelForm):
	class Meta:
		model = MapMarker
		widgets = {
			'bubble_en': Textarea(attrs={'class': 'mceNoEditor'}),
			'bubble_ru': Textarea(attrs={'class': 'mceNoEditor'}),
			'bubble_de': Textarea(attrs={'class': 'mceNoEditor'}),
			'bubble_ko': Textarea(attrs={'class': 'mceNoEditor'}),
			'bubble_zh': Textarea(attrs={'class': 'mceNoEditor'}),
			'title_en': Textarea(attrs={'class': 'mceNoEditor', 'rows': '3'}),
			'title_ru': Textarea(attrs={'class': 'mceNoEditor', 'rows': '3'}),
			'title_de': Textarea(attrs={'class': 'mceNoEditor', 'rows': '3'}),
			'title_ko': Textarea(attrs={'class': 'mceNoEditor', 'rows': '3'}),
			'title_zh': Textarea(attrs={'class': 'mceNoEditor', 'rows': '3'}),
		}

class MapMarkerAdmin(BaseAdmin, HTMLMixin):
	model = MapMarker
	form = MapMarkerForm
	list_filter = ('map_layer', 'publish')
	list_display = ('publish', '__unicode__', 'colour')
	fieldsets = (
		(
		_('Marker Settings'), {
			'fields': ('map_layer', 'colour', 'direction', 'image', 'link', 'publish'),
			'classes': ('collapse', 'open',),
			},
		),
		(
		_('Location'), {
			'fields': ('location_en', 'location_ru', 'location_de', 'location_ko', 'location_zh'),
			'classes': ('collapse', 'closed'),
			},
		),
		(
		_('Title'), {
			'fields': ('title_en', 'title_ru', 'title_de', 'title_ko', 'title_zh'),
			'classes': ('collapse', 'closed'),
			},
		),
		(
		_('Bubble'), {
			'fields': ('bubble_en', 'bubble_ru', 'bubble_de', 'bubble_ko', 'bubble_zh'),
			'classes': ('collapse', 'closed'),
			},
		),
	)

class BottomPictureAdAdmin(SortableAdmin):
	fieldsets = (
		(
			_('Settings'), {
				'fields': ('image', 'publish'),
				'classes': ('collapse', 'open'),
				},
		),
		(
			_('Title'), {
				'fields': ('title_en', 'title_ru', 'title_de', 'title_ko', 'title_zh'),
				'classes': ('collapse', 'closed'),
				},
		),
		(
			_('Link'), {
				'fields': ('link_en', 'link_ru', 'link_de', 'link_ko', 'link_zh'),
				'classes': ('collapse', 'closed'),
				},
		),
		(
			_('Link Text'), {
				'fields': ('link_text_en', 'link_text_ru', 'link_text_de', 'link_text_ko', 'link_text_zh'),
				'classes': ('collapse', 'closed'),
				},
		),
		(
			_('Description'), {
				'fields': ('description_en', 'description_ru', 'description_de', 'description_ko', 'description_zh'),
				'classes': ('collapse', 'closed'),
				},
		),
	)

class BottomLinkInline(SortableStackedInline):
	model = BottomLink
	fieldsets = (
		(
			_('Settings'), {
				'fields': ('publish', 'position', 'bottom_link_group'),
				'classes': ('collapse', 'open'),
				},
		),
		(
			_('Link'), {
				'fields': ('link_en', 'link_ru', 'link_de', 'link_ko', 'link_zh'),
				'classes': ('collapse', 'closed'),
				},
		),
		(
		_('Link text'), {
			'fields': ('link_text_en', 'link_text_ru', 'link_text_de', 'link_text_ko', 'link_text_zh'),
			'classes': ('collapse', 'closed'),
			},
		),
	)
	extra = 0

class BottomLinkGroupAdmin(SortableAdmin):
	inlines = (BottomLinkInline,)
	fieldsets = (
		(
			_('Settings'), {
				'fields': ('publish',),
				'classes': ('collapse', 'open'),
				},
		),
		(
			_('Title'), {
				'fields': ('title_en', 'title_ru', 'title_de', 'title_ko', 'title_zh'),
				'classes': ('collapse', 'closed'),
				},
		),
	)

class GoogleMarkerForm(ModelForm):
	class Meta:
		widgets = {
			'blurb_en': Textarea(attrs={'class': 'mceNoEditor', 'rows': '3'}),
			'blurb_ru': Textarea(attrs={'class': 'mceNoEditor', 'rows': '3'}),
			'blurb_de': Textarea(attrs={'class': 'mceNoEditor', 'rows': '3'}),
			'blurb_ko': Textarea(attrs={'class': 'mceNoEditor', 'rows': '3'}),
			'blurb_zh': Textarea(attrs={'class': 'mceNoEditor', 'rows': '3'}),
		}

class GoogleMarkerAdmin(SortableAdmin):
	form = GoogleMarkerForm
	model = GoogleMarker
	fieldsets = (
		(
			_('Settings'), {
				'fields': ('map', 'link', 'image', 'publish',),
				'classes': ('collapse', 'open'),
				},
		),
		(
			_('title'), {
				'fields': ('title_en', 'title_ru', 'title_de', 'title_ko', 'title_zh'),
				'classes': ('collapse', 'closed'),
				},
		),
		(
			_('subtitle'), {
				'fields': ('subtitle_en', 'subtitle_ru', 'subtitle_de', 'subtitle_ko', 'subtitle_zh'),
				'classes': ('collapse', 'closed'),
				},
		),
		(
			_('Blurb'), {
				'fields': ('blurb_en', 'blurb_ru', 'blurb_de', 'blurb_ko', 'blurb_zh'),
				'classes': ('collapse', 'closed'),
				},
		),
		(
			_('Coordinates'), {
				'fields': ( 'lat', 'lng',),
				'classes': ('collapse', 'closed'),
				},
		),
	)

admin.site.register(Section, SectionAdmin)
admin.site.register(MainBanner, MainBannerAdmin)
admin.site.register(Page, TechPageAdmin)
admin.site.register(MapLayer, MapLayerAdmin)
admin.site.register(SubSection, SubSectionAdmin)
admin.site.register(MapMarker, MapMarkerAdmin)
admin.site.register(MapLayerHotspot, MapLayerHotspotAdmin)
admin.site.register(PageTextGraphic, PageTextGraphicAdmin)
admin.site.register(PageTextMap, PageTextMapAdmin)
admin.site.register(PageTwoColumnsTextGraphic, PageTwoColumnsTextGraphicAdmin)
admin.site.register(BottomPictureAd, BottomPictureAdAdmin)
admin.site.register(BottomLinkGroup, BottomLinkGroupAdmin)
admin.site.register(PageContacts, PageContactsAdmin)
admin.site.register(GoogleMarker, GoogleMarkerAdmin)
admin.site.register(EnabledLanguage, SortableAdmin)