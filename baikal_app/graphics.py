from datetime import datetime
from random import randint
import re
from PIL import Image, ImageFont, ImageDraw
import cairo
from os import path as ospath
#from django.conf \
import os
from cairotext.text import create_cairo_font_face_for_file
import settings
from textograf.text import _red, _green, _blue, _alpha

def web_to_rgba(text):
	if re.match('#[0-9a-fA-F]{6}$', text):
		return int('0xff%s%s%s' % (text[5:7], text[3:5], text[1:3]), 16)
	return 0

class PilCompatImageSurface(object):
	"""
	A wrapper object which allows the underlying cairo surface to be saved to a file interchangeably with PIL's Image
	"""

	def __init__(self, surface):
		if isinstance(surface, cairo.ImageSurface):
			self._surface = surface
		else:
			raise ValueError('Value passed to the PilCompatImageSurface should be an instance of cairo.ImageSurface')

	def save(self, filename):
		f = file(filename, 'wb')
		self._surface.write_to_png(f)

def make_banner(image, title, text, star_text, locale, text_color):
	"""
	Return a banner image resource
	image - Original image FileObject
	title, text, star_text - as is
	locale - locale code
	"""
	LOCALE_SETTINGS = {
		'zh': {
			'title_font': ospath.join(settings.TEXTOGRAF_FONT_PATH, 'FZY4JW.TTF'),
			'text_font': ospath.join(settings.TEXTOGRAF_FONT_PATH, 'DroidSansFallback.ttf'),
			'star_font': ospath.join(settings.TEXTOGRAF_FONT_PATH, 'FZY4JW.TTF'),
			'title_size': 46,
			'text_size': 22,
			'title_margin': 32,
			'title_top': 12,
			'title_spacer': -5,
			'text_spacer': 20,
			'star_size': 13,
		},
		'ko': {
			'title_font': ospath.join(settings.TEXTOGRAF_FONT_PATH, 'DroidSansFallback.ttf'),
			'text_font': ospath.join(settings.TEXTOGRAF_FONT_PATH, 'DroidSansFallback.ttf'),
			'star_font': ospath.join(settings.TEXTOGRAF_FONT_PATH, 'DroidSansFallback.ttf'),
			'title_size': 38,
			'text_size': 22,
			'title_margin': 32,
			'title_top': 22,
			'title_spacer': -10,
			'text_spacer': 20,
			'star_size': 13,
		},
	}
	RESOURCE_PATH = ospath.join(ospath.dirname(__file__), 'resources')
	WHITE_ROUND_DIAMETER = 266
	ROUND_DIAMETER = 246
	PICTURE_SIZE = (280,266)

	STAR_FONT = ospath.join(settings.TEXTOGRAF_FONT_PATH, 'officina_ctt_bold.ttf')
	STAR_SIZE = 13
	TITLE_MARGIN = 32
	TITLE_FONT = ospath.join(settings.TEXTOGRAF_FONT_PATH, 'OfficinaSerifBlackC.otf')
	TITLE_TOP = 22
	TITLE_SPACER = -20
	TITLE_SIZE = 48
	TEXT_FONT = ospath.join(settings.TEXTOGRAF_FONT_PATH, 'OSR45__C.TTF')
	TEXT_SIZE = 24
	TEXT_SPACER = 14
	if locale in LOCALE_SETTINGS:
		if 'title_size' in LOCALE_SETTINGS[locale]:
			TITLE_SIZE = LOCALE_SETTINGS[locale]['title_size']
		if 'text_size' in LOCALE_SETTINGS[locale]:
			TEXT_SIZE = LOCALE_SETTINGS[locale]['text_size']
		if 'title_font' in LOCALE_SETTINGS[locale]:
			TITLE_FONT = LOCALE_SETTINGS[locale]['title_font']
		if 'text_font' in LOCALE_SETTINGS[locale]:
			TEXT_FONT = LOCALE_SETTINGS[locale]['text_font']
		if 'title_margin' in LOCALE_SETTINGS[locale]:
			TITLE_MARGIN = LOCALE_SETTINGS[locale]['title_margin']
		if 'title_top' in LOCALE_SETTINGS[locale]:
			TITLE_TOP = LOCALE_SETTINGS[locale]['title_top']
		if 'title_spacer' in LOCALE_SETTINGS[locale]:
			TITLE_SPACER = LOCALE_SETTINGS[locale]['title_spacer']
		if 'text_spacer' in LOCALE_SETTINGS[locale]:
			TEXT_SPACER = LOCALE_SETTINGS[locale]['text_spacer']
		if 'star_font' in LOCALE_SETTINGS[locale]:
			STAR_FONT = LOCALE_SETTINGS[locale]['star_font']
		if 'star_size' in LOCALE_SETTINGS[locale]:
			STAR_SIZE = LOCALE_SETTINGS[locale]['star_size']

	img = Image.open(image)
	img.load()
	# Crop the image to a square
	if img.size[0] > img.size[1]:
		size_diff = (img.size[0] - img.size[1]) / 2
		img = img.crop((size_diff, 0, size_diff + img.size[1], img.size[1]))
	elif img.size[1] > img.size[0]:
		size_diff = (img.size[1] - img.size[0]) / 2
		img = img.crop((0, size_diff, img.size[0], size_diff + img.size[0]))
	if img.size[0] != ROUND_DIAMETER and img.size[1] != ROUND_DIAMETER:
		img = img.resize((ROUND_DIAMETER, ROUND_DIAMETER), Image.ANTIALIAS)
	mask = Image.open(ospath.join(RESOURCE_PATH, 'mask.png')).convert('L')
	newimg = Image.open(ospath.join(RESOURCE_PATH, 'circle_white.png'))
	newimg.paste(img, ((WHITE_ROUND_DIAMETER - ROUND_DIAMETER) / 2, (WHITE_ROUND_DIAMETER - ROUND_DIAMETER) / 2), mask)
	picture_layer = Image.new('RGBA', PICTURE_SIZE)
	picture_layer.paste(newimg, (0,0))
	star_layer = Image.new('RGBA', PICTURE_SIZE)
	star_layer.paste(Image.open(ospath.join(RESOURCE_PATH, 'yellow_star.png')), (158, 0))
	out = Image.composite(star_layer, picture_layer, star_layer)
	del star_layer, newimg, img, picture_layer
	__centered_text(out, star_text.upper(), STAR_FONT, 58, 218, STAR_SIZE, -2)
	title_font = ImageFont.truetype(TITLE_FONT, TITLE_SIZE)
	sub_font = ImageFont.truetype(TEXT_FONT, TEXT_SIZE)
	width = 0
	title = title.replace('\r', '')
	text = text.replace('\r', '')
	for line in title.split('\n'):
		width = max(title_font.getsize(line)[0], width)
	for line in text.split('\n'):
		width = max(sub_font.getsize(line)[0], width)
	final = Image.new('RGBA', (WHITE_ROUND_DIAMETER + TITLE_MARGIN + width, WHITE_ROUND_DIAMETER), color=0x00ffffff)
	final.paste(out, (0, 0))
	del out
	draw = ImageDraw.Draw(final)
	top = TITLE_TOP
	for line in title.split('\n'):
		draw.text((WHITE_ROUND_DIAMETER + TITLE_MARGIN, top), line, font=title_font, fill='#ffffff')
		top += title_font.getsize(line)[1] + TITLE_SPACER
	top += TEXT_SPACER
	for line in text.split('\n'):
		draw.text((WHITE_ROUND_DIAMETER + TITLE_MARGIN, top), line, font=sub_font, fill=text_color)
		top += sub_font.getsize(line)[1] - 2
	return final

def cairo_make_banner(image, title, text, star_text, locale, text_color):
	"""
	Return a banner image resource
	image - Original image FileObject
	title, text, star_text - as is
	locale - locale code
	"""
	LOCALE_SETTINGS = {
		'zh': {
			'title_font': ospath.join(settings.TEXTOGRAF_FONT_PATH, 'FZY4JW.TTF'),
			'text_font': ospath.join(settings.TEXTOGRAF_FONT_PATH, 'DroidSansFallback.ttf'),
			'star_font': ospath.join(settings.TEXTOGRAF_FONT_PATH, 'FZY4JW.TTF'),
			'title_size': 46,
			'text_size': 22,
			'title_margin': 32,
			'title_top': 12,
			'title_interline': -5,
			'text_spacer': 20,
			'star_size': 13,
		},
		'ko': {
			'title_font': ospath.join(settings.TEXTOGRAF_FONT_PATH, 'DroidSansFallback.ttf'),
			'text_font': ospath.join(settings.TEXTOGRAF_FONT_PATH, 'DroidSansFallback.ttf'),
			'star_font': ospath.join(settings.TEXTOGRAF_FONT_PATH, 'DroidSansFallback.ttf'),
			'title_size': 38,
			'text_size': 22,
			'title_margin': 32,
			'title_top': 22,
			'title_interline': -10,
			'text_spacer': 20,
			'star_size': 13,
		},
	}
	RESOURCE_PATH = ospath.join(ospath.dirname(__file__), 'resources')
	WHITE_ROUND_DIAMETER = 266
	ROUND_DIAMETER = 246
	PICTURE_SIZE = (280,266)

	STAR_FONT = ospath.join(settings.TEXTOGRAF_FONT_PATH, 'officina_ctt_bold.ttf')
	STAR_SIZE = 13
	STAR_INTERLINE = 5.0
	STAR_COLOR = 0xffffffff
	STAR_CENTER_X = 218.0
	STAR_CENTER_Y = 60.0
	TITLE_MARGIN = 32.0
	TITLE_FONT = ospath.join(settings.TEXTOGRAF_FONT_PATH, 'OfficinaSerifBlackC.otf')
	TITLE_TOP = 22.0
	TITLE_SIZE = 48
	TITLE_COLOR = 0xffffffff
	TITLE_INTERLINE = 4.0
	TEXT_FONT = ospath.join(settings.TEXTOGRAF_FONT_PATH, 'OSR45__C.TTF')
	TEXT_SIZE = 24
	TEXT_SPACER = 14.0 # Spacer between the title and the text
	TEXT_COLOR = web_to_rgba(text_color)
	TEXT_INTERLINE = 4.0

	if locale in LOCALE_SETTINGS:
		if 'title_size' in LOCALE_SETTINGS[locale]:
			TITLE_SIZE = LOCALE_SETTINGS[locale]['title_size']
		if 'text_size' in LOCALE_SETTINGS[locale]:
			TEXT_SIZE = LOCALE_SETTINGS[locale]['text_size']
		if 'title_font' in LOCALE_SETTINGS[locale]:
			TITLE_FONT = LOCALE_SETTINGS[locale]['title_font']
		if 'text_font' in LOCALE_SETTINGS[locale]:
			TEXT_FONT = LOCALE_SETTINGS[locale]['text_font']
		if 'title_margin' in LOCALE_SETTINGS[locale]:
			TITLE_MARGIN = LOCALE_SETTINGS[locale]['title_margin']
		if 'title_top' in LOCALE_SETTINGS[locale]:
			TITLE_TOP = LOCALE_SETTINGS[locale]['title_top']
		if 'title_spacer' in LOCALE_SETTINGS[locale]:
			TITLE_INTERLINE = LOCALE_SETTINGS[locale]['title_interline']
		if 'text_spacer' in LOCALE_SETTINGS[locale]:
			TEXT_SPACER = LOCALE_SETTINGS[locale]['text_spacer']
		if 'star_font' in LOCALE_SETTINGS[locale]:
			STAR_FONT = LOCALE_SETTINGS[locale]['star_font']
		if 'star_size' in LOCALE_SETTINGS[locale]:
			STAR_SIZE = LOCALE_SETTINGS[locale]['star_size']

	img = Image.open(image)
	img.load()
	# Crop the image to a square
	if img.size[0] > img.size[1]:
		size_diff = (img.size[0] - img.size[1]) / 2
		img = img.crop((size_diff, 0, size_diff + img.size[1], img.size[1]))
	elif img.size[1] > img.size[0]:
		size_diff = (img.size[1] - img.size[0]) / 2
		img = img.crop((0, size_diff, img.size[0], size_diff + img.size[0]))
	if img.size[0] != ROUND_DIAMETER and img.size[1] != ROUND_DIAMETER:
		img = img.resize((ROUND_DIAMETER, ROUND_DIAMETER), Image.ANTIALIAS)
	mask = Image.open(ospath.join(RESOURCE_PATH, 'mask.png')).convert('L')
	newimg = Image.open(ospath.join(RESOURCE_PATH, 'circle_white.png'))
	newimg.paste(img, ((WHITE_ROUND_DIAMETER - ROUND_DIAMETER) / 2, (WHITE_ROUND_DIAMETER - ROUND_DIAMETER) / 2), mask)
	picture_layer = Image.new('RGBA', PICTURE_SIZE)
	picture_layer.paste(newimg, (0,0))
	star_layer = Image.new('RGBA', PICTURE_SIZE)
	star_layer.paste(Image.open(ospath.join(RESOURCE_PATH, 'yellow_star.png')), (158, 0))
	out = Image.composite(star_layer, picture_layer, star_layer)
	tmpfile = os.path.join(ospath.dirname(__file__), 'resources', 'tmp', '%s%.5d.png' % (datetime.now().strftime('%Y%m%d%H%M%S%f'), randint(0, 99999),))
	out.save(tmpfile, 'PNG')
	del star_layer, newimg, img, picture_layer
	test_srf = cairo.ImageSurface(cairo.FORMAT_ARGB32, 0, 0)
	test_ctx = cairo.Context(test_srf)
	title_font = create_cairo_font_face_for_file(TITLE_FONT)
	text_font = create_cairo_font_face_for_file(TEXT_FONT)
	star_font = create_cairo_font_face_for_file(STAR_FONT)
	t_width = title_line_height = text_line_height = 0
	test_ctx.set_font_face(title_font)
	test_ctx.set_font_size(TITLE_SIZE)
	title_lines = title.splitlines()
	for line in title_lines:
		t_metrics = test_ctx.text_extents(line)
		t_width = max(t_width, t_metrics[2])
		title_line_height = max(title_line_height, t_metrics[3])
	t_height = (title_line_height + TITLE_INTERLINE) * len(title_lines) + TITLE_INTERLINE
	test_ctx.set_font_face(text_font)
	test_ctx.set_font_size(TEXT_SIZE)
	text_lines = text.splitlines()
	for line in text_lines:
		t_metrics = test_ctx.text_extents(line)
		t_width = max(t_width, t_metrics[2])
		text_line_height = max(text_line_height, t_metrics[3])
	t_height += len(text_lines) * (text_line_height + TEXT_INTERLINE) - TEXT_INTERLINE
	gfx_srf = cairo.ImageSurface.create_from_png(tmpfile)
	final_srf = cairo.ImageSurface(cairo.FORMAT_ARGB32, WHITE_ROUND_DIAMETER + int(TITLE_MARGIN) + int(t_width), max(WHITE_ROUND_DIAMETER, int(t_height)))
	final_ctx = cairo.Context(final_srf)
	final_ctx.set_source_surface(gfx_srf)
	final_ctx.rectangle(0, 0, WHITE_ROUND_DIAMETER + TITLE_MARGIN + t_width, max(WHITE_ROUND_DIAMETER, t_height))
	final_ctx.fill()
	del gfx_srf
	os.unlink(tmpfile)
	final_ctx.set_source_rgba(_red(STAR_COLOR), _green(STAR_COLOR), _blue(STAR_COLOR), _alpha(STAR_COLOR))
	final_ctx.set_font_face(star_font)
	final_ctx.set_font_size(STAR_SIZE)
	star_line_height = 0
	star_lines = star_text.upper().splitlines()
	for line in star_lines:
		s_metrics = final_ctx.text_extents(line)
		star_line_height = max(star_line_height, s_metrics[3])
	star_baseline = STAR_CENTER_Y - (len(star_lines) * (STAR_INTERLINE + star_line_height) / 2) - STAR_INTERLINE / 2
	for line in star_lines:
		s_metrics = final_ctx.text_extents(line)
		final_ctx.move_to(STAR_CENTER_X - (s_metrics[2] / 2), star_baseline + star_line_height)
		final_ctx.show_text(line)
		star_baseline += star_line_height + STAR_INTERLINE
	final_ctx.set_font_face(title_font)
	final_ctx.set_font_size(TITLE_SIZE)
	top = TITLE_TOP + title_line_height
	final_ctx.set_source_rgba(_red(TITLE_COLOR), _green(TITLE_COLOR), _blue(TITLE_COLOR), _alpha(TITLE_COLOR))
	for line in title_lines:
		final_ctx.move_to(WHITE_ROUND_DIAMETER + TITLE_MARGIN, top)
		final_ctx.show_text(line)
		top += title_line_height + TITLE_INTERLINE
	top += TEXT_SPACER
	final_ctx.set_font_face(text_font)
	final_ctx.set_font_size(TEXT_SIZE)
	final_ctx.set_source_rgba(_red(TEXT_COLOR), _green(TEXT_COLOR), _blue(TEXT_COLOR), _alpha(TEXT_COLOR))
	for line in text_lines:
		final_ctx.move_to(WHITE_ROUND_DIAMETER + TITLE_MARGIN, top)
		final_ctx.show_text(line)
		top += text_line_height + TEXT_INTERLINE
	return PilCompatImageSurface(final_srf)


def wet_floor(image):
	"""
	Add a wet floor effect to an image.
	:param image: a FileObject of the image

	Function currently disabled
	"""
	# source = Image.open(image)
	# final = Image.new('RGBA', (source.size[0], source.size[1]), color=0x00eaf2f5)
	# final.paste(source, (0, 0, source.size[0], source.size[1]))
	# return final
	source = Image.open(image)
	mask = Image.open(ospath.join(ospath.dirname(__file__), 'resources', 'mokry_pol.png'))
	final_mask = Image.new('RGBA', (source.size[0], mask.size[1]))
	mask_width = 0
	while mask_width < source.size[0]:
		final_mask.paste(mask, (mask_width, 0))
		mask_width += mask.size[0]
	reflection = source.transform(final_mask.size, Image.EXTENT, (0, source.size[1], source.size[0], source.size[1] - final_mask.size[1]))
	final = Image.new('RGBA', (source.size[0], source.size[1] + mask.size[1]), color=0x00eaf2f5)
	final.paste(source, (0, 0, source.size[0], source.size[1]))
	final.paste(reflection, (0, source.size[1], reflection.size[0], source.size[1] + reflection.size[1]), mask=final_mask)
	return final

def __centered_text(image, text, font, top, left, size, interline=0):
	font = ImageFont.truetype(font, size)

	# First pass to determine height
	text_height = 0
	text = text.replace('\r', '')
	for line in text.split('\n'):
		text_height += font.getsize(line)[1] + interline
	draw = ImageDraw.Draw(image)
	line_top = top - (text_height / 2)
	for line in text.split('\n'):
		line_size = font.getsize(line)
		draw.text((left - (line_size[0] / 2), line_top), line, font=font, fill='#ffffff')
		line_top += line_size[1] + interline