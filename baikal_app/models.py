# coding: utf-8
import re
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.safestring import mark_safe
from easymode.i18n.decorators import I18n
from filebrowser.fields import FileBrowseField
from positions import PositionField
from pytils.translit import slugify
from django.utils.translation import ugettext_lazy as _

class OrderedModel(models.Model):
	position = PositionField(_(u'Position'))

	class Meta:
		abstract = True
		ordering = ('position',)

@I18n('title', 'subtitle', 'icon_title', 'icon_text', 'icon_star_text')
class MainBanner(OrderedModel):
	TEXT_COLOR = (
		('#000000', _('Black')),
		('#ffffff', _('White'))
	)
	title = models.TextField(_(u'Title'), max_length=255)
	subtitle = models.TextField(_(u'Subtitle'), null=True, blank=True)
	image = FileBrowseField(_(u'Image'), max_length=255)
	icon = FileBrowseField(_(u'Image for the info block on the left'), max_length=255)
	icon_title = models.TextField(_(u'Info block title text'))
	icon_text = models.TextField(_(u'Info block main text'))
	icon_text_color = models.CharField(_(u'Info block main text color'), max_length=255, default='#ffffff',
									   choices=TEXT_COLOR)
	icon_star_text = models.TextField(_('Info block text in the star'))
	url = models.CharField(_(u'Redirection url'), max_length=1024, blank=True, null=True)
	publish = models.BooleanField(_(u'Publish'), default=False)

	class Meta:
		verbose_name = _(u'Main banner')
		verbose_name_plural = _(u'Main banners')
		ordering = ('position',)

	def __unicode__(self):
		return self.title
	
	@models.permalink
	def get_absolute_url(self):
		return 'baikal_app.views.slide_banner', (self.pk, )

@I18n('title')
class Section(OrderedModel):
	title = models.CharField(_(u'Title'), max_length=255)
	publish = models.BooleanField(_(u'Publish'), default=False)
	width = models.PositiveIntegerField(_(u'Top menu width'), help_text=_(u'Indicated in pixels'))

	class Meta:
		verbose_name = _(u'Section')
		verbose_name_plural = _(u'Sections')
		ordering = ('position',)

	def __unicode__(self):
		return self.title

	def ordered_subsections(self):
		# TODO: Почему-то .filter() не работает...
		return self.subsection_set.order_by('position')

	def menu_image_filename(self):
		return 'menu_%s.png' % slugify(self.title).replace('-', '_')

@I18n('title')
class SubSection(OrderedModel):
	section = models.ForeignKey(Section, verbose_name=_(u'Section'))
	title = models.CharField(_(u'Title'), max_length=255)
	publish = models.BooleanField(_(u'Publish'), default=False)

	class Meta:
		verbose_name = _(u'Subsection')
		verbose_name_plural = _(u'Subsections')
		ordering = ('position',)

	def __unicode__(self):
		return self.title

	@models.permalink
	def get_absolute_url(self):
		if self.page_set.count():
			return 'baikal_app.views.handle_page', (self.page_set.order_by('position')[0].alias,)

@I18n('title', 'html_keywords', 'html_description', 'content', 'image_title', 'short_title', 'image')
class Page(OrderedModel):
	PAGE_TYPES = (
		('text_graphic', _(u'Text & graphics')),
		('text_map', _(u'Text & map')),
		('twocoltext_graphic', _(u'Two column text & graphics')),
		('text', _(u'Text only')),
		('graphic', _(u'Graphics only')),
		('contacts', _(u'Contacts')),
		('tech', _(u'!Technical layout!')),
		('googlemap', _(u'Google map')),
	)
	IMAGE_COLORS = (
		('0xffffffff', _(u'White')),
		('0xff000000', _(u'Black')),
	)
	sub_section = models.ForeignKey(SubSection, verbose_name=_(u'Subsection'), null=True, blank=True)
	short_title = models.CharField(_(u'Short title for menu'), max_length=255, null=True, blank=True)
	title = models.CharField(_(u'Page title'), max_length=255)
	html_keywords = models.CharField(_(u'HTML keywords'), max_length=1024, null=True, blank=True)
	html_description = models.CharField(_(u'HTML description'), max_length=1024, null=True, blank=True)
	page_type = models.CharField(_(u'Page type'), choices=PAGE_TYPES, max_length=255, default='text_graphic')
	content = models.TextField(_(u'Content'), blank=True, null=True)
	image = FileBrowseField(_(u'Image'), max_length=255, blank=True, null=True,
							help_text=_(u'Image on the right hand side of the page'))
	image_title = models.TextField(_(u'Image title'), blank=True, null=True)
	image_title_color = models.CharField(_(u'Image title color'), max_length=255, null=True, blank=True,
										 choices=IMAGE_COLORS, default=u'0xff000000')
	alias = models.SlugField(_(u'Page address'))
	publish = models.BooleanField(_(u'Publish'))

	class Meta:
		verbose_name = _(u'Page')
		verbose_name_plural = _(u'Pages')
		ordering = ('position',)

	def __unicode__(self):
		return self.title

	@models.permalink
	def get_absolute_url(self):
		return 'baikal_app.views.handle_page', (self.alias,)

	@models.permalink
	def get_title_image(self):
		return 'baikal_app.views.page_title', (str(self.pk),)

	@models.permalink
	def get_subtitle_image(self):
		return 'baikal_app.views.page_subtitle', (str(self.pk),)

	@models.permalink
	def get_slide_title_url(self):
		return 'baikal_app.views.slide_title', (str(self.pk),)

	def menu_title(self):
		u"""
		Заголовок для меню
		"""
		return self.short_title if self.short_title else self.title

@I18n('title', 'image', 'shift_x', 'shift_y')
class MapLayer(models.Model):
	page = models.ForeignKey(Page, verbose_name=_(u'Map'))
	title = models.CharField(_(u'Button title'), max_length=255)
	image = FileBrowseField(_(u'Image'), max_length=255)
	shift_x = models.IntegerField(_(u'X shift right'), default=0)
	shift_y = models.IntegerField(_(u'Y shift down'), default=0)
	publish = models.BooleanField(_(u'Publish'))
	position = PositionField(_(u'Position'), collection=('page',))

	class Meta:
		verbose_name = _(u'Map layer')
		verbose_name_plural = _(u'Map layers')
		ordering = ('position',)

	def __unicode__(self):
		return u'%s "%s" %s "%s"' % (_('Layer'), self.title, _('for map'), self.page.title,)

	def enabled_markers(self):
		return self.mapmarker_set.filter(publish=True)

	def all_markers(self):
		return self.mapmarker_set.all()

	def enabled_hotspots(self):
		return self.maplayerhotspot_set.filter(publish=True)

@I18n('coords', 'image')
class MapLayerHotspot(models.Model):
	SHAPES = (
		('rect', _('Rectangle')),
		('circle', _('Circle')),
		('poly', _('Polygon')),
	)
	map_layer = models.ForeignKey(MapLayer, verbose_name=_(u'Map layer'))
	shape = models.CharField(_('Shape'), max_length=64, choices=SHAPES)
	coords = models.CharField(_('Coordinates'), max_length=1024)
	image = FileBrowseField(_('Image replacement'), max_length=255)
	publish = models.BooleanField(_(u'Publish'), default=False)

	class Meta:
		verbose_name = _('Map Layer Hotspot')
		verbose_name_plural = _('Map Layer Hotspots')

	def __unicode__(self):
		return u'%s %s' % (_('Hotspot for'), self.map_layer)

@I18n('location', 'bubble', 'title')
class MapMarker(models.Model):
	COLOURS = (
		('nature_brown', _(u'Nature: Brown')),
		('nature_blue', _(u'Nature: Blue')),
		('culture_cherry', _(u'Culture: Cherry')),
		('culture_green', _(u'Culture: Green')),
		('culture_orange', _(u'Culture: Orange')),
	)
	DIRECTIONS = (
		('left', _(u'Left'),),
		('right', _(u'Right'),),
	)
	map_layer = models.ForeignKey(MapLayer, verbose_name=_(u'Map Layer'))
	colour = models.CharField(_(u'Color'), max_length=64, choices=COLOURS)
	location = models.CharField(_(u'Location'), max_length=12,
								help_text=_(u'Location should be indicated in the following format: x;y'))
	bubble = models.TextField(_(u'Bubble content'), blank=True, null=True)
	title = models.CharField(_(u'Title [hint]'), max_length=255, blank=True, null=True)
	link = models.CharField(_(u'Link'), max_length=1024, null=True, blank=True)
	direction = models.CharField(_(u'Marker direction'), max_length=64, choices=DIRECTIONS, default='left')
	image = FileBrowseField(_(u'Marker Image'), max_length=1024, null=True, blank=True)
	publish = models.BooleanField(_(u'Publish'), default=False)

	class Meta:
		verbose_name = _(u'Map layer marker')
		verbose_name_plural = _(u'Map layer markers')

	def __unicode__(self):
		if self.title:
			uc = u'%s %s' % (_('Marker'), self.title)
		else:
			uc = u'%s (%s,%s)' %(_('Marker at'), self.__indexed_location(0), self.__indexed_location(1))
		return uc.replace('&nbsp;', ' ')

	def __indexed_location(self, index):
		if self.location and ';' in self.location:
			try:
				result = int(self.location.split(';')[index])
			except ValueError:
				return None
			return result
		return None

	def x(self):
		return self.__indexed_location(0)

	def y(self):
		return self.__indexed_location(1)

@I18n('title', 'link', 'link_text', 'description')
class BottomPictureAd(models.Model):
	title = models.CharField(_('Title'), max_length=255)
	image = FileBrowseField(_('Image'), max_length=1024)
	link = models.CharField(_('Link'), max_length=1024)
	link_text = models.CharField(_('Link text'), max_length=1024)
	description = models.TextField(_('Description'))
	position = PositionField(_('Position'))
	publish = models.BooleanField(_('Publish'), default=False)

	def __unicode__(self):
		return self.title

	class Meta:
		verbose_name = _('Bottom ad')
		verbose_name_plural = _('Bottom ads')
		ordering = ('position',)

	@models.permalink
	def get_picture_url(self):
		return 'baikal_app.views.main_picture', (self.pk,)

@I18n('title')
class BottomLinkGroup(models.Model):
	title = models.CharField(_('Title'), max_length=255)
	position = PositionField(_('Position'))
	publish = models.BooleanField(_('Publish'), default=False)

	class Meta:
		verbose_name = _('Bottom Link Group')
		verbose_name_plural = _('Bottom Link Groups')
		ordering = ('position',)

	def __unicode__(self):
		return self.title

	def published_links(self):
		return self.bottomlink_set.filter(publish=True)

@I18n('link', 'link_text')
class BottomLink(models.Model):
	bottom_link_group = models.ForeignKey(BottomLinkGroup, verbose_name=_('Bottom Link Group'))
	link_text = models.CharField(_('Link text'), max_length=255)
	link = models.CharField(_('Link'), max_length=1024)
	position = PositionField(_('Position'))
	publish = models.BooleanField(_('Publish'), default=False)

	class Meta:
		verbose_name = _('Bottom Link')
		verbose_name_plural = _('Bottom Links')
		ordering = ('position',)

	def __unicode__(self):
		return self.link_text

class EnabledLanguage(models.Model):
	language_code = models.CharField(_('Language Code'), max_length=2, editable=False)
	label = models.CharField(_('Label'), max_length=32)
	publish = models.BooleanField(_('Publish'), default=False)
	position = PositionField()

	def __unicode__(self):
		return self.language_code

	class Meta:
		verbose_name = _('Enabled Language')
		verbose_name_plural = _('Enabled Languages')
		ordering = ('position',)

def validate_coordinate(value):
	if not re.match(r'^\d+\.\d+$', value):
		raise ValidationError(_('Only values of the form 123.123123123123 are allowed'))

@I18n('title', 'subtitle', 'blurb')
class GoogleMarker(models.Model):
	map = models.ForeignKey(Page, verbose_name=_('Map'))
	lat = models.CharField(_('Latitude'), max_length=32, validators=[validate_coordinate,])
	lng = models.CharField(_('Longitude'), max_length=32, validators=[validate_coordinate,])
	title = models.CharField(_('Title'), max_length=255)
	subtitle = models.CharField(_('Subtitle'), max_length=512)
	blurb = models.TextField(_('Blurb'))
	image = FileBrowseField(_('Image'), max_length=512)
	link = models.CharField(_('Link'), max_length=512)
	position = PositionField()
	publish = models.BooleanField(_('Publish'), default=False)
	
	def __unicode__(self):
		return '%s: %s' % (self.title, self.subtitle, )

	class Meta:
		verbose_name = _('Google Marker')
		verbose_name_plural = _('Google markers')
		ordering = ('position',)

# Admin proxy models
class PageTextGraphic(Page):
	class Meta:
		proxy = True
		verbose_name = _(u'Page with text and graphics')
		verbose_name_plural = _(u'Pages with text and graphics')

	def save(self, *args, **kwargs):
		self.page_type = 'text_graphic'
		super(PageTextGraphic, self).save(*args, **kwargs)

class PageTextMap(Page):
	class Meta:
		proxy = True
		verbose_name = _(u'Page with text and a map')
		verbose_name_plural = _(u'Pages with text and a map')

	def save(self, *args, **kwargs):
		self.page_type = 'text_map'
		super(PageTextMap, self).save(*args, **kwargs)

class PageTwoColumnsTextGraphic(Page):
	class Meta:
		proxy = True
		verbose_name = _(u'Page with two columns of text and graphics')
		verbose_name_plural = _(u'Pages with two columns of text and graphics')

	def save(self, *args, **kwargs):
		self.page_type = 'twocoltext_graphic'
		super(PageTwoColumnsTextGraphic, self).save(*args, **kwargs)

class PageContacts(Page):
	class Meta:
		proxy = True
		verbose_name = _(u'Contacts page')
		verbose_name_plural = _(u'Contacts pages')

	def save(self, *args, **kwargs):
		self.page_type = 'contacts'
		super(PageContacts, self).save(*args, **kwargs)
