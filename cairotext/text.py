import ctypes
from textwrap import wrap
import cairo
from django.http import HttpResponse
import os
import re
from django.conf import settings

def first_font(path):
	success = False
	font_re = re.compile('\w+\.[ot]tf$', re.I)
	for f in os.listdir(path):
		if font_re.match(f):
			success = True
			break
	if not success:
		raise Exception("Font directory %s does not contain any fonts" % path)
	return f

FONT_PATH = getattr(settings, 'TEXTOGRAF_FONT_PATH', os.path.join(os.path.dirname(__file__), 'fonts'))
def_font = getattr(settings, 'TEXTOGRAF_DEFAULT_FONT', 'FreeSansBold.otf')
if not(def_font in os.listdir(FONT_PATH)):
	def_font = first_font(FONT_PATH)
DEFAULT_FONT = def_font
ANTIALIAS_PADDING = 1 # Extra padding 1 pixel on each side

def _red(color):
	return color & 0xff

def _green(color):
	return (color & 0xff00) / 0x100

def _blue(color):
	return (color & 0xff0000) / 0x10000

def _alpha(color):
	return (color & 0xff000000) / 0x1000000

class PycairoContext(ctypes.Structure):
	_fields_ = [("PyObject_HEAD", ctypes.c_byte * object.__basicsize__),
		("ctx", ctypes.c_void_p),
		("base", ctypes.c_void_p)]

_initialized = False
def create_cairo_font_face_for_file (filename, faceindex=0, loadoptions=0):
	global _initialized
	global _freetype_so
	global _cairo_so
	global _ft_lib
	global _surface

	CAIRO_STATUS_SUCCESS = 0
	FT_Err_Ok = 0

	if not _initialized:

		# find shared objects
		try:
			platform = os.uname()[0]
		except IndexError:
			raise Exception('Unsupported Operating System')
		if platform == 'Darwin':
			ftlib = 'libfreetype.dylib'
			cairolib = 'libcairo.dylib'
		elif platform == 'Linux':
			ftlib = 'libfreetype.so.6'
			cairolib = 'libcairo.so.2'
		else:
			raise Exception('Unsupported Operating System')
		_freetype_so = ctypes.CDLL (ftlib)
		_cairo_so = ctypes.CDLL (cairolib)

		_cairo_so.cairo_ft_font_face_create_for_ft_face.restype = ctypes.c_void_p
		_cairo_so.cairo_ft_font_face_create_for_ft_face.argtypes = [ ctypes.c_void_p, ctypes.c_int ]
		_cairo_so.cairo_set_font_face.argtypes = [ ctypes.c_void_p, ctypes.c_void_p ]
		_cairo_so.cairo_font_face_status.argtypes = [ ctypes.c_void_p ]
		_cairo_so.cairo_status.argtypes = [ ctypes.c_void_p ]

		# initialize freetype
		_ft_lib = ctypes.c_void_p ()
		if FT_Err_Ok != _freetype_so.FT_Init_FreeType (ctypes.byref (_ft_lib)):
			raise Exception('Error initialising FreeType library.')

		_surface = cairo.ImageSurface (cairo.FORMAT_A8, 0, 0)
		_initialized = True

	# create freetype face
	ft_face = ctypes.c_void_p()
	cairo_ctx = cairo.Context (_surface)
	cairo_t = PycairoContext.from_address(id(cairo_ctx)).ctx

	if FT_Err_Ok != _freetype_so.FT_New_Face (_ft_lib, filename, faceindex, ctypes.byref(ft_face)):
		raise Exception("Error creating FreeType font face for " + filename)

	# create cairo font face for freetype face
	cr_face = _cairo_so.cairo_ft_font_face_create_for_ft_face (ft_face, loadoptions)
	if CAIRO_STATUS_SUCCESS != _cairo_so.cairo_font_face_status (cr_face):
		raise Exception("Error creating cairo font face for " + filename)

	_cairo_so.cairo_set_font_face (cairo_t, cr_face)
	if CAIRO_STATUS_SUCCESS != _cairo_so.cairo_status (cairo_t):
		raise Exception("Error creating cairo font face for " + filename)

	face = cairo_ctx.get_font_face ()

	return face

def text_as_graphic(text, **kwargs):
	"""
	Draw `text` on a graphic canvas and return the resulting HttpResponse.

	Possible keyword arguments are::

		bg - background color (defaults to transparent white)
		fg - foreground color (defaults to non-transparent black)
		size - font size (defaults to 14)
		font_file - A TTF or OTF font filename. CAIROTEXT_FONT_PATH must be set in the settings when this setting is used
		extra_interline - an extra space between the lines (defaults to 0)
		word_wrap - maximum number of characters (hyphenation not supported) defaults to False
		extra_bottom - add space at the bottom (defaults to 0)
		image_size - resulting image size as a tuple containing width and height (defaults to text size) !TODO
		align - horizontal text alignment one of the strings 'left', 'right', 'center' (defaults to 'left')
		padding - tuple of horizontal and vertical padding (defaults to (0,0,))
		baseline_shift - move the line higher or lower
		right_padding - extra space on the right

	returns an HttpResponse with the PNG image.
	"""
	#Initialize parameters
	bg = kwargs.get('bg', 0x00ffffff)
	fg = kwargs.get('fg', 0xff000000)
	size = kwargs.get('size', 14)
	font_file = kwargs.get('font_file', DEFAULT_FONT)
	extra_interline = kwargs.get('extra_interline', 0)
	word_wrap = kwargs.get('word_wrap', False)
	extra_bottom = kwargs.get('extra_bottom', 0)
	image_size = kwargs.get('image_size', False)
	align = kwargs.get('align', 'left')
	padding = kwargs.get('padding', (0,0,))
	baseline_shift = kwargs.get('baseline_shift', 0)
	right_padding = kwargs.get('right_padding', 0)
	font_face = create_cairo_font_face_for_file(os.path.join(FONT_PATH, font_file))
	if word_wrap:
		text = wrap(text, word_wrap)
	else:
		text = text.splitlines()
	check_surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, 0, 0)
	check_ctx = cairo.Context(check_surface)
	check_ctx.set_font_face(font_face)
	check_ctx.set_antialias(cairo.ANTIALIAS_SUBPIXEL)
	check_ctx.set_font_size(size)
	max_line_height = 0
	if not image_size:
		width = padding[0]
		height = padding[1]
		for line in text:
			extents = check_ctx.text_extents(line)
			# extents is a tuple with the following values:
			# x_bearing, y_bearing - distance of the top left corner of the text bounds from the current position
			# width, height
			# x_advance, y_advance - where will the cursor should be advanced after drawing text to continue the line
			width = max(width, extents[2])
			max_line_height = max(max_line_height, extents[3])
		height += (max_line_height + extra_interline) * len(text) + extra_bottom
	else:
		width, height = image_size
		for line in text:
			extents = check_ctx.text_extents(line)
			max_line_height = max(max_line_height, extents[3])
	out_srf = cairo.ImageSurface(cairo.FORMAT_ARGB32, int(width) + ANTIALIAS_PADDING * 2, int(height) + ANTIALIAS_PADDING * 2)
	out_ctx = cairo.Context(out_srf)
	out_ctx.set_font_face(font_face)
	out_ctx.set_font_size(size)
	out_ctx.set_antialias(cairo.ANTIALIAS_SUBPIXEL)
	out_ctx.set_source_rgba(_red(fg), _green(fg), _blue(fg), _alpha(fg))
	current_y = padding[1] + ANTIALIAS_PADDING
	for num, line in enumerate(text):
		extents = out_ctx.text_extents(line)
		y_shift = max_line_height + extra_interline if num else extents[3]
		current_y += y_shift
		if align == 'center':
			current_x = (width - extents[2]) / 2
		elif align == 'right':
			current_x = width - extents[2] - padding[0] - ANTIALIAS_PADDING
		else:
			current_x = padding[0] + ANTIALIAS_PADDING
		out_ctx.move_to(current_x, current_y)
		out_ctx.show_text(line)
	response = HttpResponse(mimetype='image/png')
	out_srf.write_to_png(response)
	return response

