from django.conf.urls.defaults import *

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
import os
import settings

admin.autodiscover()

js_info_dict = {
    'packages': ('baikal.baikal_app',),
}

urlpatterns = patterns('',
    (r'^admin/', include(admin.site.urls)),
    (r'^jsi18n/$', 'django.views.i18n.javascript_catalog', js_info_dict),
    (r'^admin/filebrowser/', include('filebrowser.urls')),
)

if 'rosetta' in settings.INSTALLED_APPS:
    urlpatterns += patterns('',
        url(r'^rosetta/', include('rosetta.urls')),
    )

urlpatterns += patterns('baikal_app.views',
    # Example:
    # (r'^baikal/', include('baikal.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # (r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    (r'^$', 'index'),
    (r'^ajax/next_banner/$', 'next_banner'),
    (r'^ajax/previous_banner/$', 'previous_banner'),
    (r'^ajax/banner/(\d+)/$', 'banner'),
    (r'^ajax/next_page/(\d+)/$', 'next_page'),
    (r'^ajax/previous_page/(\d+)/$', 'previous_page'),
    (r'^ajax/page/(\d+)/$', 'page'),
    (r'^ajax/get_page_id/$', 'get_page_id'),
    (r'^ajax/get_banner_id/$', 'get_banner_id'),

    (r'^resources/page_title/(\d+).png$', 'page_title'),
    (r'^resources/page_content/(\d+).png$', 'page_subtitle'),
    (r'^resources/slide_title/(\d+).png$', 'slide_title'),
    (r'^resources/google/$', 'iframe_google'),
    (r'^resources/slide_banners/(\d+).png$', 'slide_banner'),
    (r'^resources/main_pictures/(\d+).png$', 'main_picture'),

    (r'^([a-z0-9_-]+)/$', 'handle_page')
)



if os.uname()[0] == "Darwin":
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve',
            {'document_root': settings.MEDIA_ROOT }),
        )