var Baikal = {};
Baikal.MAX_WIDTH = 1250;
Baikal.MAX_HEIGHT = 648;
$(document).ready(function(){

	/* ipad */
	if ((navigator.userAgent.indexOf('iPad') != -1)) {
		$('body').css('min-height', $(window).height()+30);
	}
	/* end ipad */

	window.animation_complete = false;
	var mw = Baikal.MAX_WIDTH;
	var mh = Baikal.MAX_HEIGHT;
	
	$(window).resize(function() {
	 	detectHeight();
        resizeAreas();
	});
	
	detectHeight();

	function detectHeight(){
	
		var ww = $('body').width();
		var wh = $('body').height();
		var ch = wh - 188;
		var cw = ww;

		if (cw < 1250 || ch < 648) {
			var temp_w = ch*mw/mh;
			var temp_h = ch;
			if (temp_w < cw) {
				$('#content').css('height', ch);
				$('#main_block').css('height', ch);
				$('#main_block').css('width', ch*mw/mh);
			} else {
				$('#main_block').css('width', cw);
				$('#main_block').css('height', cw*mh/mw);
			}
			$('#main_pic .pic_bgr img').css('height', '100%');
		}
	}

	//for animation
	$('#main_pic .pic_top').css('left','60px');
	if ( !$.browser.msie ) {
		$('#main_pic .pic_top').css('opacity', 0);
		$('#main_pic .pic_bgr').css('opacity', 0);
	} else {
		$('#main_pic .pic_top').hide();
		$('#main_pic .pic_bgr').hide();
	}
	
	//for two collumns
	$('.map_top_info_2cols').find('hr').nextAll('p').wrapAll('<span/>');
	$('.map_top_info_2cols hr').hide();
	    
    // click on image_links
    $('.image_links li').click(function(){
    	window.location = $(this).find('a').attr('href');
    });
});

function onImages() {
    if (!window.animation_complete)
    {	//animation
        window.animation_complete = true;
		
        if ( !$.browser.msie ) {
            $('#main_pic .pic_top')
            .delay(400)
            .animate({
                'left': '-=30px',
                'opacity': '1'
                }, 300);
             $('#main_pic .pic_bgr').animate({
            	'opacity': '1'
            	}, 200);
        } else {
            $('#main_pic .pic_top')
            .delay(400)
            .css('left', '-=30px')
            .fadeIn(300);
            $('#main_pic .pic_bgr').fadeIn(200);
        }
    }
}

function load_content_from_url(url, fnComplete) {
    window.animation_complete = false;
    $.get(url, function(data) {
        $('#main_pic').html(data);
        if (fnComplete) {
            if ($.browser.msie) {
                fnComplete();
            } else {
                var reg = /([a-zA-Z/0-9_-]+\.(jpg|jpeg|png))/g;
                var image_counter=0;
                var images=[];
                while(r = reg.exec(data)) {
                    images[image_counter] = r[0];
                    image_counter++;
                }
                window.images_to_load = image_counter;
                for (i=0;i<image_counter;i++) {
                    $('<img />').attr('src', images[i]).load(function(){
                        window.images_to_load--;
                        if (window.images_to_load==0) { fnComplete(); }
                    });
                }
            }
        }
    });
}

function next_page(lang, id) {
    load_content_from_url('/'+lang+'/ajax/next_page/'+id+'/', function(){ onImages();
        $.get('/'+lang+'/ajax/get_page_id/', function(data) {
           set_page_indicator(data);
        });
    });
}

function previous_page(lang, id) {
    load_content_from_url('/'+lang+'/ajax/previous_page/'+id+'/', function() { onImages();
        $.get('/'+lang+'/ajax/get_page_id/', function(data) {
            set_page_indicator(data);
        });
    });
}

function load_page(lang, id) {
    load_content_from_url('/'+lang+'/ajax/page/'+id+'/', function(){ onImages();
        set_page_indicator(id);
    });
}

function next_banner(lang) {
    d = new Date().getTime();
    load_content_from_url('/' + lang + '/ajax/next_banner/?q=' + d, function() {
        $.get('/'+lang+'/ajax/get_banner_id/', function(id) { set_banner_indicator(id); });
    });
}

function previous_banner(lang) {
    d = new Date().getTime();
    load_content_from_url('/' + lang + '/ajax/previous_banner/?q=' + d, function() {
        $.get('/'+lang+'/ajax/get_banner_id/', function(id) { set_banner_indicator(id); });
    });
}

function load_banner(lang, id) {
    load_content_from_url('/'+lang+'/ajax/banner/'+id+'/', function() { set_banner_indicator(id) });
}

function set_banner_indicator(id) {
    prepare_for_animation();
    $('.banner_dot.selected').removeClass('selected');
    $('#banner_dot_'+id).addClass('selected');
    window.location.hash = '#'+id;
}

function set_page_indicator(id) {
    $('.page_dot.selected').removeClass('selected');
    $('#page_dot_'+id).addClass('selected');
    window.location.hash = '#' + id;
}

function prepare_for_animation() {
    $('#main_pic .pic_top').css('left','60px');
    if ( !$.browser.msie ) {
        $('#main_pic .pic_top').fadeTo(300, 0);
        $('#main_pic .pic_bgr').fadeTo(300, 0, function() { onImages(); });
    } else {
        $('#main_pic .pic_top').hide();
        $('#main_pic .pic_bgr').hide(function() { onImages(); });
    }
}

function resizeAreas() {
    var cz = current_zoom();
    if (cz != 1) {
        $('area').each(function(e) {
           var orig = $(this).attr('rel').split(',').map(function(e) { return Math.ceil(e * cz); }).join(',');
           $(this).attr('coords', orig);
        });
        $('div.item_marker').each(function(e){
            var xy = $(this).attr('rel').split(';');
            if (xy.length == 2) {
                $(this).css('top', (xy[1] * cz) + 'px');
                $(this).css('right', (xy[0] * cz) + 'px');
            }
        });
    }
}

function current_zoom() {
    cw = $('body').width();
    ch = $('body').height() - 188;
    if (cw < Baikal.MAX_WIDTH || ch < Baikal.MAX_HEIGHT) {
        return Math.min(cw/Baikal.MAX_WIDTH, ch/Baikal.MAX_HEIGHT);
    }
    return 1;
}

function switch_lang(to) {
    window.location = window.location.href.replace(/^(http:\/\/.*?\/)\w{2}(\/.*)$/, '$1' + to + '$2');
}