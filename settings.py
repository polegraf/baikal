# coding: utf8
# Django settings for baikal project.
import os, sys

DEBUG = True
TEMPLATE_DEBUG = DEBUG
PROJECT_DIR = os.path.dirname(__file__)

ADMINS = (
    # ('Your Name', 'your_email@domain.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': os.path.join(PROJECT_DIR, 'db.db'),                      # Or path to database file if using sqlite3.
        'USER': '',                      # Not used with sqlite3.
        'PASSWORD': '',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Europe/Moscow'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en'

LANGUAGES = (
	('en', 'en'),
	('de', 'de'),
	('ru', u'ру'),
	('zh', u'中文'),
	('ko', u'한국어')
)

FALLBACK_LANGUAGES = {
	'en': [],
	'de': ['en',],
	'ru': ['en',],
	'zh': ['en',],
	'ko': ['zh', 'en',],
}
USE_SHORT_LANGUAGE_CODES = True

TEXTOGRAF_FONT_PATH = os.path.join(PROJECT_DIR, 'fonts')
TEXTOGRAF_DEFAULT_FONT = 'OfficinaSerifBlackC.otf'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/"
MEDIA_ROOT = os.path.join(PROJECT_DIR, 'media')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash if there is a path component (optional in other cases).
# Examples: "http://media.lawrence.com", "http://example.com/media/"
MEDIA_URL = '/media/'

# URL prefix for admin media -- CSS, JavaScript and images. Make sure to use a
# trailing slash.
# Examples: "http://foo.com/media/", "/media/".
ADMIN_MEDIA_PREFIX = '/admin_media/'

#Filebrowser configuration options
FILEBROWSER_MEDIA_ROOT = os.path.join(MEDIA_ROOT)
FILEBROWSER_MEDIA_URL = os.path.join(MEDIA_URL)
FILEBROWSER_URL_FILEBROWSER_MEDIA = os.path.join(ADMIN_MEDIA_PREFIX, 'filebrowser/')
FILEBROWSER_DIRECTORY = 'images/'
FILEBROWSER_VERSIONS = {
        'fb_thumb': {'verbose_name': 'Admin Thumbnail', 'width': 60, 'height': 60, 'opts': 'crop upscale'},
        'fb_thumb_95': {'verbose_name': 'Admin Thumbnail', 'width': 95, 'height': 95, 'opts': 'crop upscale'},
        'small': {'verbose_name': 'Small (120px)', 'width': 120, 'height': '', 'opts': ''},
        'map_thumb': {'verbose_name': 'Map Preview', 'width': 150, 'height': 150, 'opts': 'crop'},
		'career_thumb': {'verbose_name': 'Иконка для карьеры', 'width': 75, 'height': 75, 'opts': 'crop'},
		'news_thumb': {'verbose_name': u'Иконка для новостей', 'width': 150, 'height': 110, 'opts': 'crop' },
		'mega_banner': {'verbose_name': u'Иконка для новостей', 'width': 708, 'height': 377, 'opts': 'crop upscale' },
		'partner_logo': {'verbose_name': u'Лого партнёра', 'width': 200, 'height': 96, 'opts': '' },
		'news_by_width': {'verbose_name': u'800 по ширине', 'width': 800, 'height': '', 'opts': '' },
		'news_by_height': {'verbose_name': u'800 по ширине', 'width': '', 'height': 800, 'opts': ''},
		'career_full': {'verbose_name': 'Большая картинка для карьеры', 'width': 150, 'height': 150, 'opts': 'crop'},
}

FILEBROWSER_ADMIN_VERSIONS = ['small', 'news_thumb', 'mega_banner']
lang_links = []
for lang in LANGUAGES:
	lang_links.append(u'<a href="#" onclick="switch_lang(\'%s\')">%s</a>' % (lang[0], lang[1]))
GRAPPELLI_ADMIN_TITLE = u'Байкал %s' % u' | '.join(lang_links)

# Make this unique, and don't share it with anybody.
SECRET_KEY = '=^*+r21a!cg#(a+ald1-2*+d_!rw8itosk)0z$7z+o@8h(#nne'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = (
	'grappelli.context_processors.admin_template_path',
	'django.contrib.auth.context_processors.auth',
	'django.core.context_processors.i18n',
	'django.core.context_processors.request',
)

MIDDLEWARE_CLASSES = (
	'localeurl.middleware.LocaleURLMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

ROOT_URLCONF = 'baikal.urls'

TEMPLATE_DIRS = (os.path.join(PROJECT_DIR,'templates'),)

INSTALLED_APPS = (
	'grappelli',
    'django.contrib.auth',
	'easymode.i18n',
	'localeurl',
	'rosetta-grappelli',
	'rosetta',
	'filebrowser',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
    'baikal.baikal_app',
)
