
Baikal Resort
===================

Web application on Django Framework ver. 1.2
https://djangoproject.com/

Requirements
-------------

Django==1.2.7  
MySQL-python==1.2.5  
PIL==1.1.7  
django-easymode==0.14.5  
django-filebrowser==3.1  
django-grappelli==2.2  
django-localeurl==1.5  
django-positions==0.4.3  
django-rosetta==0.6.8  
pytils==0.2.3  
cairo==1.14.0  
py2cairo==1.10.0  