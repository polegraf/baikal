from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()

@register.filter('textsplit')
@stringfilter
def split(val, delim):
	'''
	Create a list by splitting the value using the delimiter
	@val - the value being split
	@delim - the delimiter
	'''
	return val.split(delim)
split.is_safe = True