import Image, ImageDraw, ImageFont
from django.conf import settings
from django.http import HttpResponse
from os import path as ospath

FONT_PATH = getattr(settings, 'TEXTOGRAF_FONT_PATH', ospath.join(ospath.dirname(__file__), 'fonts'))
DEFAULT_FONT = getattr(settings, 'TEXTOGRAF_DEFAULT_FONT', 'FreeSansBold.otf')

def word_wrap(sentence, char_count):
	"""
	Wrap a line of text, so that a length of a line won't exceed char_count characters
	"""
	title = [[]]
	for word in sentence.split():
		if len(' '.join(title[-1])) + len(word) > char_count:
			title.append([word])
		else:
			title[-1].append(word)
	output = []
	for line in title:
		output.append(' '.join(line))
	return '\n'.join(output)

def text_as_graphic(text, **kwargs):
	"""
	Draw `text` on a graphic canvas and return the resulting HttpResponse.

	Possible keyword arguments are::

		bg - background color (defaults to transparent white)
		fg - foreground color (defaults to non-transparent black)
		size - font size (defaults to 14)
		font_file - A TTF or OTF font filename. TEXTOGRAF_FONT_PATH must be set in the settings when this setting is used
		extra_interline - an extra space between the lines (defaults to 0)
		word_wrap - maximum number of characters (hyphenation not supported) defaults to False
		extra_bottom - add space at the bottom (defaults to 0)
		image_size - resulting image size as a tuple containing width and height (defaults to text size) !TODO
		align - horizontal text alignment one of the strings 'left', 'right', 'center' (defaults to 'left')
		padding - tuple of horizontal and vertical padding (defaults to (0,0,))
		baseline_shift - move the line higher or lower

	returns an HttpResponse with the PNG image.
	"""
	if not text:
		image = Image.new('RGBA', (1,1))
		response = HttpResponse(mimetype='image/png')
		image.save(response, 'PNG')
		return response
	bg = kwargs.get('bg', 0x00ffffff)
	fg = kwargs.get('fg', 0xff000000)
	size = kwargs.get('size', 16)
	text_align = kwargs.get('align', 'left')
	baseline_shift = kwargs.get('baseline_shift', 0)
	text_padding = kwargs.get('padding', (0,0,))
	font_file = ospath.join(FONT_PATH, kwargs['font_file']) if kwargs.get('font_file', False) else ospath.join(
        ospath.dirname(__file__), 'fonts', DEFAULT_FONT)
	extra_interline = kwargs.get('extra_interline', 0)
	if kwargs.get('word_wrap', False):
		text = word_wrap(text, kwargs['word_wrap'])
	lines = text.splitlines()
	width = 0
	height = kwargs.get('extra_bottom', 0)
	interline = int(size * -0.2) + 2 + extra_interline
	font = ImageFont.truetype(font_file, size)
	for line in lines:
		line_size = font.getsize(line)
		width = max(width, line_size[0])
		height += line_size[1] + interline
	if kwargs.get('image_size', False):
		try:
			width, height = kwargs['image_size']
		except TypeError, ValueError:
			pass
	image = Image.new('RGBA', (width, height), bg)
	draw = ImageDraw.Draw(image)
	y = text_padding[1]
	x = 0
	for line in lines:
		line_size = font.getsize(line)
		if text_align == 'right':
			x = width - line_size[0] - text_padding[0]
		elif text_align == 'center':
			x = (width - line_size[0])/2
		draw.text((x, y + baseline_shift), line, font=font, fill=fg)
		y += line_size[1] + interline
	response = HttpResponse(mimetype='image/png')
	image.save(response, 'PNG')
	return response

def captcha(text, **kwargs):
	"""
	Draw a CAPTCHA image based on the provided `text`.

	Possible keyword arguments include::

		bg (RGBA hex) - preferred background color (default: *white*)
		fg (RGBA hex) - preferred text color (default: *black*)
		size (int) - base text size (default: *24*)
		extra_height - adjust the height of the image (default: *15*)
		extra_width - adjust the width of the image (default: *number of chars x 10*)
		color_variation (RGBA hex) - each byte indicates how much deviation allowed (default: *0x11111111*)
		noise_level (int) - how much noise allowed in percentage 0 - 100 (default: *some noise*)
		size_variation (int) - how much the size of the letters is allowed to be changed (default: *3*)
		rotation (int) - maximum rotation allowed on the symbols (default: *35*)
		char_spacing (int) - space between characters (default: *5*)

	"""
	import random
	bg = kwargs.get('bg', 0xffffffff)
	fg = kwargs.get('fg', 0xff000000)
	size = kwargs.get('size', 24)
	color_variation = kwargs.get('color_variation', 0xff555555)
	extra_height = kwargs.get('extra_height', 10)
	extra_width = kwargs.get('extra_width', len(text)*10)
	size_variation = kwargs.get('size_variation', 3)
	char_spacing = kwargs.get('char_spacing', 10)
	rotation = kwargs.get('rotation', 35)
	font_file = ospath.join(FONT_PATH, kwargs['font_file']) if kwargs.get('font_file', False) else ospath.join(
        ospath.dirname(__file__), 'fonts', DEFAULT_FONT)
	fonts = [ImageFont.truetype(font_file, fs) for fs in range(size - size_variation, size + size_variation)]
	max_w, max_h = fonts[-1].getsize(text)
	max_w += char_spacing * len(text)
	main_image = Image.new('RGBA', (max_w + extra_width, max_h + extra_height), bg)
	start = 0
	for ch in text:
		fnt = fonts[random.choice(range(size_variation*2))]
		char_w, char_h = fnt.getsize(ch)
		char_box = max(char_w, char_h)
		image = Image.new('RGBA', (char_box, char_box), bg)
		mask = Image.new('L', (char_box, char_box))
		draw = ImageDraw.Draw(image)
		mdraw = ImageDraw.Draw(mask)
		new_red, new_green, new_blue, new_alpha = _red(fg), _green(fg), _blue(fg), _alpha(fg)
		for f, c in ((_red, 'new_red'), (_green, 'new_green'), (_blue, 'new_blue'), (_alpha, 'new_alpha')):
			if f(color_variation):
				new = min(255, max(0, eval(c) + random.choice(range(-f(color_variation), f(color_variation)))))
				exec('%s = %i' % (c, new))
		draw.text((0 , 0), ch, font=fnt, fill=(new_red, new_green, new_blue, new_alpha))
		mdraw.text((0,0), ch, font=fnt, fill=255)
		angle = random.choice(range(-rotation, rotation))
		y = random.choice(range(max_h - char_box)) if max_h - char_box else 0
		main_image.paste(image.rotate(angle, resample=Image.BICUBIC, expand=1), (start, y), mask.rotate(angle, resample=Image.BICUBIC, expand=1))
		del draw, image
		start += (char_box + random.choice(range(-char_spacing*2, char_spacing))) if char_spacing else char_box
	response = HttpResponse(mimetype='image/png')
	main_image.save(response, 'PNG')
	return response

def _red(color):
	return color & 0xff

def _green(color):
	return (color & 0xff00) / 0x100

def _blue(color):
	return (color & 0xff0000) / 0x10000

def _alpha(color):
	return (color & 0xff000000) / 0x1000000