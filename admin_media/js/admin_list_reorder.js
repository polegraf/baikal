django.jQuery(document).ready(function() {
    // Set this to the name of the column holding the position
    if (!enable_column_sortable('position'))
        enable_column_sortable('index_ordering', '# на гл.');
    else
        hide_column_sortable('index_ordering', '# на гл.');

    function enable_column_sortable(pos_field, suffix) {

        var pos_col = hide_column_sortable(pos_field, suffix);
        if (!pos_col) return false;
        // Determine sorted column and order
        var sorted = django.jQuery('.changelist-results thead th.sorted');
        var sorted_col = django.jQuery('.changelist-results thead th').index(sorted);
        var sort_order = sorted.hasClass('descending') ? 'desc' : 'asc';

        if (!filters_are_okay()) {
            // Check if the apprpriate filter is enabled
            remove_list_sort_column();
            console.info("The dataset should be filtered appropriately.");
            return false;
        }

        if (sorted_col != pos_col) {
            // Sorted column is not position column, bail out
            console.info("Sorted column is not %s, bailing out", pos_field);
            return false;
        }

        // Store the sorting shift, whchever is smallest
        var top = parseInt(django.jQuery('div.changelist-results td:last-child>input').val());
        var bottom = parseInt(django.jQuery('div.changelist-results td:last-child>input').val());
        window.sgtour_admin_list_items_shift = (top > bottom) ? bottom : top;
        django.jQuery('.changelist-results tbody tr').css('cursor', 'move');

        // Make tbody > tr sortable
        django.jQuery('.changelist-results tbody').sortable({
            axis: 'y',
            items: 'tr',
            cursor: 'move',
            update: function(event, ui) {
                var item = ui.item;
                var items = django.jQuery(this).find('tr').get();

                if (sort_order == 'desc') {
                    // Reverse order
                    items.reverse();
                }

                django.jQuery(items).each(function(index) {
                    var shifted = index + window.sgtour_admin_list_items_shift;
                    pos_td = django.jQuery(this).children()[pos_col];
                    input = django.jQuery(pos_td).children('input').first();
                    label = django.jQuery(pos_td).children('strong').first();

                    input.attr('value', shifted);
                    label.text(shifted);
                });

                // Update row classes
                django.jQuery(this).find('tr').removeClass('row1').removeClass('row2');
                django.jQuery(this).find('tr:even').addClass('row1');
                django.jQuery(this).find('tr:odd').addClass('row2');

                // Blink the button
                django.jQuery('input[name="_save"]').animate({'opacity': 0.1}, 100,
                        function(){ django.jQuery(this).animate({'opacity': 1}, 200,
                        function(){ django.jQuery(this).animate({'opacity': 0.2}, 300,
                        function(){ django.jQuery(this).animate({'opacity': 1}, 500) }
                        )})});
            }
        });
        return true;
    }

    function hide_column_sortable(pos_field, suffix) {
        // Determine the column number of the position field
        pos_col = null;

        cols = django.jQuery('.changelist-results tbody tr:first').children();

        for (i = 0; i < cols.length; i++) {
            inputs = django.jQuery(cols[i]).find('input[name$=' + pos_field + ']');

            if (inputs.length > 0) {
                // Found!
                pos_col = i;
                break;
            }
        }

        if (pos_col == null) {
            return false;
        }

        // Some visual enhancements
        header = django.jQuery('.changelist-results thead tr').children()[pos_col];
        django.jQuery(header).css('width', '1em');
        var replacement_text = suffix || '#';
        django.jQuery(header).children('a').text(replacement_text);

        // Hide position field
        django.jQuery('.changelist-results tbody tr').each(function(index) {
            pos_td = django.jQuery(this).children()[pos_col];
            input = django.jQuery(pos_td).children('input').first();
            input.hide();

            label = django.jQuery('<strong>' + input.attr('value') + '</strong>');
            django.jQuery(pos_td).append(label);
        });
        return pos_col;
    }
});

function filters_are_okay() {
    // Check if it is appropriate to enable the list soting feature
    // For some models, sorting will only be available if a filter is applied
    var FILTER_STRINGS = {
        'city': 'region__webobject_ptr__exact',
        'region': 'country__webobject_ptr__exact',
        'hotel': 'city__webobject_ptr__exact',
        'tour': 'tour_group__id__exact',
        'specialoffer': 'tour__id__exact',
        'excursion': 'city__webobject_ptr__exact',
        'webpage': 'parent__webobject_ptr__exact'
    };
    var model = window.location.pathname.split('/')[3];
    // if we are an some weird location, skip
    if (!model) return false;
    if (model in FILTER_STRINGS) {
        return (django.jQuery('.filter_choice').val().indexOf(FILTER_STRINGS[model] + '=') > 0);
    }
    // Sorting is enabled for all models not listed here
    return true;
}

function remove_list_sort_column() {
    django.jQuery('div.changelist-results tr>td:last-child').hide();
    django.jQuery('th.sortable:last-child').hide();
}